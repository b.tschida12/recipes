---
draft: false
title: "Artichoke Appetizers"
author: 
date: 2021-01-25T20:16:56-05:00
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings: 34
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 20 # in minutes
calories: # in kcal
---

## Ingredients:

- 8 Rhodes Texas Rolls thawed and risen (or use frozen puff pastry, thawed 40 minutes)
- 14 ounce can artichoke hearts
- 1 cup mayonnaise
- 1 cup grated Monterey jack cheese
- 1/2 cup grated Parmesan cheese
- fresh parsley

# Instructions:

1. Drain artichokes and chop into small pieces.
2. In a bowl, mix artichokes, mayonnaise and cheeses until well combined. Put into saucepan, cook until cheese is melted and bubbly
3. Press rolls together and roll into a 12x17-inch rectangle. Place on large sprayed baking sheet. Spread artichoke mixture over dough going all the way to the edges. Let rise for 15 minutes.
4. Bake at 350°F 20 minutes or until light golden brown. Garnish with parsley. Cut into strips 1x6-inches each. Makes 34 strips.
