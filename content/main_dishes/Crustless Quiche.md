---
draft: false
title: "Crustless Quiche"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
2 T grated parmesan

5 eggs

1 c chopped onions

1 c diced ham or bacon

1 T butter

8 oz shredded cheese

2 c Half and Half

Up to 1 c veggies

Preheat oven 425.

Butter quiche dish, sprinkle with parmesan cheese. ( or use muffin tins)

Cook onions with ham or bacon about 5 minutes. Add veggies if using. Spread in dish, then evenly sprinkle cheese on top.

Whisk together eggs, half & half and ½ t pepper, pour over cheese.

Bake 30-35 minutes
