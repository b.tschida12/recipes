---
draft: false
title: "Guinness braised short ribs"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
GUINNESS BRAISED

BEEF SHORT RIBS

Original recipe by Guinness, adapted by Kristi. Enough for an 8-10 person dinner party.

12, 2-3" bone-in beef short ribs (5 pounds)

32 ounces Tomato Juice

3 cans Guinness Draught

2 cups Beef Stock

4 Carrots, diced

6 Ribs Celery, diced

3 Yellow Onions, chopped

4 sprigs fresh Thyme

Oil for browning meat

Flat-leaf parsley for garnish

Preheat oven to 300 degrees F. Season short ribs generously with salt and pepper. Coat heavy pot with oil and pan-sear short ribs in batches. When meat is seared to an amber-brown color, transfer to a roasting pan. In the same pan, sauté carrots, celery and onion until soft and beginning to brown (10 minutes). Add to roasting pan. Add Guinness to pan and de-glaze over medium heat. Pour this liquid over the short-ribs in the roasting pan. Add tomato juice, beef stock and thyme.

​

Cover tightly with lid or foil and braise in preheated oven for 3 hours. Remove short ribs from roasting pan and transfer to a pot. Strain liquid and put about half into a saucepan. Skim fat. Simmer over medium-high until reduced to create a sauce. Pour over short-ribs and serve. Garnish with a razzle-dazzle of parsley.
