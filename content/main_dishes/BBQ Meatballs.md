---
draft: false
title: "BBQ Meatballs"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
BBQ'D Meatballs  
  
1 1/2 lbs ground beef  
3/4 cup quick oatmeal  
1 cup milk  
1 1/2 tsp each salt & pepper  
1 tsp minced garlic  
3 Tbsp minced dried onions  
Sauce:  
1 cup ketchup  
1/2 cup water  
1 Tbsp Worcestershire sauce  
3 Tbsp packed brown sugar  
1 Tbsp Sriracha sauce  
  
Mix ingredients for meatballs and using small ice cream/ cookie scoop form into balls and place in a 9x13 baking dish. Mix the sauce ingredients and pour over the meatballs. Bake at 350 degrees for 45 min. Do not need to brown first. Then place in crock pot on low to hold for party etc. if you make bigger 1/2 cup entree size you must bake them one hour.  
  
Author: Janice Ingle
