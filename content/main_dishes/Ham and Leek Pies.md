---
draft: false
title: "Ham and Leek Pies"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients

1/4 cup butter, cubed

4 cups sliced leeks (white portion only)

1/2 pound sliced fresh mushrooms

3 medium carrots, sliced

1/2 cup all-purpose flour

1-1/4 cups 2% milk

1-1/4 cups vegetable broth

1-3/4 cups cubed fully cooked ham

2 tablespoons minced fresh parsley

1/4 to 1/2 teaspoon ground nutmeg

Dash pepper

1 sheet frozen puff pastry, thawed

1 large egg, lightly beaten

Directions

1\. Preheat oven to 425°. In a large saucepan, heat butter over medium-high heat. Add leeks, mushrooms and carrots; cook and stir until tender.

2\. Stir in flour until blended. Gradually stir in milk and broth. Bring to a boil over medium heat, stirring constantly; cook and stir or until thickened, about 2 minutes. Remove from heat; stir in ham, parsley, nutmeg and pepper.

3\. On a lightly floured surface, unfold puff pastry; roll to 1/4-in. thickness. Using a 10-oz. ramekin as a template, cut out 4 tops for pies. Fill 4 greased 10-oz. ramekins with leek mixture; top with pastry. Cut slits in pastry. Brush tops with egg.

4\. Bake 18-22 minutes or until golden brown. Let stand 5 minutes before serving.

Can use sweet onion in place of leeks
