---
draft: false
title: "Cyndi Ingle_s Corned Beef"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1 package corned beef brisket (I like flat cut)

1 orange, sliced

1 onion, sliced

2 stalks celery, sliced

1 carrot, sliced

1/4 cup water

2 Tbs pickling spice (many corned beef briskets come with a packet of pickling spice)

Place the corned beef brisket in a medium bowl and cover with water. Soak

for ½ hour to remove the salt, then drain. Line a 13x9 baking dish or

roasting pan with foil, leaving extra foil over edges to seal closed over

the meat. Put the brisket on the foil and arrange the slices of orange,

onion, celery & carrot over top. Pour the water and sprinkle the pickling

spices over top. Seal tight and bake at 300° for 4 hours.

You can also do the same in a slow cooker.

Author Note: For more than one piece of brisket just add more of the

vegetables and spices. Bake the same amount of time.
