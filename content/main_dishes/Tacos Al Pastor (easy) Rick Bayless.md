---
draft: false
title: "Tacos Al Pastor (easy) Rick Bayless"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**Grilled Pork Tacos al Pastor**

**FROM RICKBAYLESS.COM**

**Servings:** 20tacos

### **Ingredients**

<table><tbody><tr class="odd"><td><ul><li><blockquote><p>A 3 1/2ounce package <a href="https://www.rickbayless.com/ingredient/achiote/">achiote</a> marinade</p></blockquote></li><li><blockquote><p>3 <a href="https://www.rickbayless.com/ingredient/chipotles-en-adobo/">canned chipotle chile en adobo</a> plus 4 tablespoons of the canning sauce</p></blockquote></li><li><blockquote><p>1/4cup vegetable or olive oil, plus a little more for the onion and pineapple</p></blockquote></li><li><blockquote><p>1 1/2pounds thin-sliced pork shoulder (about 1/4 inch)</p></blockquote></li><li><blockquote><p>Salt</p></blockquote></li><li><blockquote><p>1medium red onion, sliced 1/4-inch thick</p></blockquote></li><li><blockquote><p>1/4of a medium pineapple, sliced into 1/4-inch thick rounds</p></blockquote></li><li><blockquote><p><strong>For Serving</strong></p></blockquote></li><li><blockquote><p>20warm corn tortillas</p></blockquote></li><li><blockquote><p>About 1 1/2 cups salsa (both red chile and green salsa are good here)</p></blockquote></li><li><blockquote><p>Chopped cilantro for garnishing the tacos</p></blockquote></li><li><blockquote><p>In a blender, combine the achiote, chiles, canning sauce, oil and ¾ cup water. Blend into a smooth marinade. With a heavy mallet pound the meat to about 1/8-inch thick. Use 1/3 of the marinade to smear over both sides of each piece of meat and sprinkle with salt. Cover and refrigerate at least an hour for the flavor to penetrate the meat. (In a container with a tight-fitting lid, refrigerate the rest of the marinade for another use.)</p></blockquote></li><li><blockquote><p>Heat a gas grill to medium-high to high or light a charcoal fire and let it burn until the coals are covered with gray ash and are very hot. Brush or spray both sides of the onion and pineapple with oil. Grill, turning regularly for several minutes, until they are richly colored and softening—you want them to still have nice texture. Cut the core out of the pineapple, then chop the onions and the tender pineapple into small pieces. Keep warm on the side of the grill.</p></blockquote></li><li><blockquote><p>Grill the meat on the very hot grill, cooking it only on one side to duplicate the delicious crusty char every one associates with tacos al pastor. When the meat is cooked, which should take about a minute, cut it into short thin slices. Serve with the onion and pineapple: I find it easiest to mix the meat together with the onion and pineapple, scoop a portion of the mixture into a warm tortilla, then top with a spoonful of salsa and a sprinkling of cilantro. I think tacos al pastor aficionados will be happy.</p></blockquote></li><li><blockquote><p>©Rick Bayless. All rights reserved.</p></blockquote></li></ul></td></tr></tbody></table>
