---
draft: false
title: "Honey Chipotle Ham"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
HONEY CHIPOTLE HAM

Ingredients

1 fully cooked bone-in ham (8 to 10 pounds)

¾ cup packed brown sugar

3 tablespoons honey

2 tablespoons cider vinegar

4 chipotle peppers in adobo sauce

3 garlic cloves, minced

1-1/2 teaspoons Dijon mustard

1 t chili powder

3/4 teaspoon ground cinnamon

3/4 teaspoon ground cumin

12 oz beer

Directions

1\. Preheat oven to 325°. Place ham on a rack in a roasting pan. Using a sharp knife, score surface of ham with 1/2-in.-deep cuts in a diamond pattern. Bake, uncovered, 1-1/2 hours.

2\. Meanwhile, for glaze, Add the following to a food processor bowl: brown sugar, honey, vinegar, chipotle chilis, garlic, mustard, chili powder, cinnamon and cumin. Process to form a thick paste. Transfer to a large pan, add the beer to the paste mixture. Bring to a boil; reduce heat and cook until mixture is reduced by half, about 15 minutes. Remove from heat. Reserve 1 cup mixture for sauce; keep warm.

3\. After 90 minutes, brush ham with some of the remaining glaze. Continue to bake, uncovered, until a thermometer reads 140°, about 30 minutes, brushing twice with additional glaze.

Serve with reserved sauce.
