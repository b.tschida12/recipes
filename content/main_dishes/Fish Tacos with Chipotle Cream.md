---
draft: false
title: "Fish Tacos with Chipotle Cream"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients:

1 c sour cream

1 chipotle chile in adobo sauce, finely chopped

2 T lime juice

1-¼ t salt, divided

2 t ground cumin

1 t chipotle chile powder

1 t salt

1-½ lb skinless cod fillets

2 T vegetable oil

12 corn or flour toritllas

1-½ c shredded green cabbage

Lime wedges

Directions:

1.  In a small bowl, combine sour cream, chipotle chile, lime juice and ¼ t salt. Stir well and set aside while you cook the fish

2.  Combine spices and remaining 1 t salt together in a small bowl. Seaons cod all over in the spice mixture

3.  Heat a large nonstick skillet over medium heat. Add oil to the pan and once hot, add cod. Pan-fry fish, turning once, until cooked through, 8-10 minutes for a 1-inch thick fillet. Transfer to a serving dish a flake fish into bite-size pieces

4.  Serve cod with warmed tortillas, cabbage, and sour cream-chipotle sauce.
