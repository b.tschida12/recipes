---
draft: false
title: "Grilled chuck steak with red wine marinade"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Thin, boneless chuck steak (1/2 – 1” thick)  
Marinade: (enough to marinate 3-4 \# of steak)  
¾ cup red wine (1 mini bottle)  
2/3 cup soy sauce  
¼ cup olive oil  
¼ cup balsamic vinegar  
2 Tablespoons Worcestershire sauce  
2 teaspoons Dijon mustard  
3 cloves garlic, minced  
1 Tablespoons brown sugar

Marinate 4 hours or overnight

Grill
