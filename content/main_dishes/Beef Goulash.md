---
draft: false
title: "Beef Goulash"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**Ingredients**

Beef for stew

⅓ c flour

4 c diced potato

2 c chopped onion

1 c sliced carrot

1 c red bell pepper

⅓ c ketchup

1 T Worcestershire sauce

2 t Hungarian sweet paprika

2 t minced garlic

1 t salt

10 oz beef broth

**Directions**

Dredge beef in flour & brown. Remove to slow cooker. Pour in 2 oz broth in pan & deglaze.

Layer potatoes, onion, carrot, & peppers in crock pot. Combine sauce & pour over

Cover & cook on low 8 hours
