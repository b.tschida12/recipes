---
draft: false
title: "Chicken Scarpariello"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
INGREDIENTS

1½ pounds fingerling potatoes, halved lengthwise

6 tablespoons extra-virgin olive oil, divided

Kosher salt, freshly ground pepper

3 links sweet Italian sausage

6 skin-on, bone-in chicken thighs

2 large onions, chopped

½ large red bell pepper, chopped

6 garlic cloves, finely grated

1 cup dry white wine

1 cup low-sodium chicken broth

½ cup chopped hot, sweet pickled Peppadew peppers in brine

¼ cup white wine vinegar

3 sprigs rosemary

Chopped parsley (for serving)

RECIPE PREPARATION

Arrange racks in upper and lower thirds of oven; preheat to 450°. Toss potatoes with 3 Tbsp. oil on a rimmed baking sheet; season with salt and pepper. Arrange cut side down and roast on lower rack until tender and cut sides are browned, 20–30 minutes; set aside.

Meanwhile, heat remaining 3 Tbsp. oil in a large skillet over medium-high. Cook sausages, turning occasionally, until browned on all sides, 6–8 minutes (they will not be fully cooked). Transfer to a plate.

Season chicken on both sides with salt and pepper. Cook in same skillet, turning occasionally, until golden brown on both sides, 8–10 minutes (they will also be undercooked). Transfer to plate with sausage.

Cook onions, bell pepper, and garlic in same skillet over medium-high heat, stirring occasionally and scraping bottom of pan, until tender and beginning to brown, 10–12 minutes. Add wine and cook, stirring occasionally, until reduced and you can no longer smell the alcohol, about 8 minutes. Add broth, peppers, vinegar, and rosemary and bring to a boil; cook until slightly reduced, about 5 minutes. Nestle chicken into onion mixture, then transfer skillet to upper rack of oven and roast chicken 10 minutes. Add sausages to skillet, pushing them into onion mixture, and continue to roast until chicken is cooked through and an instant-read thermometer inserted into thickest part of thigh registers 165°, 5–10 minutes.

Top with parsley and serve with roasted potatoes alongside.
