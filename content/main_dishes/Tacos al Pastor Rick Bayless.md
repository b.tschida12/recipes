---
draft: false
title: "Tacos al Pastor Rick Bayless"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
<table><tbody><tr class="odd"><td><ul><li><blockquote><p><strong>FOR THE MARINADE AND MEAT</strong></p></blockquote></li><li><blockquote><p>8 garlic cloves, peeled</p></blockquote></li><li><blockquote><p>1/2 cup (2 counces) powdered ancho chile</p></blockquote></li><li><blockquote><p>1/2teaspoon<a href="https://www.rickbayless.com/ingredient/mexican-cinnamon/">ground cinnamon, preferably Mexican canela</a></p></blockquote></li><li><blockquote><p>1/4teaspoon fresh black pepper</p></blockquote></li><li><blockquote><p>1/3teaspoon cumin</p></blockquote></li><li><blockquote><p>1teaspoon<a href="https://www.rickbayless.com/ingredient/mexican-oregano/">dried oregano, preferably Mexican</a></p></blockquote></li><li><blockquote><p>3tablespoons vinegar (I like apple cider vinegar)</p></blockquote></li><li><blockquote><p>Salt</p></blockquote></li><li><blockquote><p>One-half of a 3 ½-ounce package <a href="https://www.rickbayless.com/ingredient/achiote/">achiote paste</a></p></blockquote></li><li><blockquote><p>1/4cup vegetable or olive oil, plus more to brush on the onions and pineapple</p></blockquote></li><li><blockquote><p>1 1/2pounds thin-sliced pork shoulder (a little thicker than ¼-inch is ideal—the kind Mexican butchers sell for making tacos al pastor)</p></blockquote></li><li><blockquote><p><strong>FOR THE SAUCE AND SALSA</strong></p></blockquote></li><li><blockquote><p>5 to 7 large (3/4 ounces) dried red chipotle chiles (often sold as moritas—you’ll want chiles that are about 2 inches long), stemmed</p></blockquote></li><li><blockquote><p>6 garlic cloves, unpeeled</p></blockquote></li><li><blockquote><p>1pound (6 to 8) tomatillos, husked and rinsed</p></blockquote></li><li><blockquote><p>1/2cup orange juice</p></blockquote></li><li><blockquote><p>1/2cup chicken broth</p></blockquote></li><li><blockquote><p><strong>FOR FINISHING THE TACOS</strong></p></blockquote></li><li><blockquote><p>1 medium white or red onion, sliced into ¼-inch rounds</p></blockquote></li><li><blockquote><p>1/4 of a medium ripe pineapple, sliced into ¼-inch-thick rounds</p></blockquote></li><li><blockquote><p>20 warm soft corn tortillas</p></blockquote></li><li><blockquote><p>Chopped cilantro for garnishing the tacos</p></blockquote></li></ul></td></tr></tbody></table>

### **INSTRUCTIONS**

*Make the marinade.* Place the garlic in a small microwaveable dish, cover with water and microwave for 1 minute. Drain and place in a blender jar. Add the ancho powder along with the spices and herb, vinegar, 1 ¼ cups boiling water and 1 ½ teaspoon salt. Blend until smooth. Measure out 1/3 cup. Scrape the remainder into a container with a tight-fitting lid and refrigerate for another use.

Scrape the red chile *adobo* mixture back into the blender jar. Break up the achiote into the blender and add the oil and 1/3 cup water. Blend until smooth.

*Marinate the meat.* Use a heavy mallet to pound the meat to about half its thickness—this will ensure tenderness. Smear or brush marinade over both sides of each slice of pork—I like more than just a light coating. Refrigerate any unused marinade for another round of taco making. Cover and refrigerate the meat for a couple of hours for the flavors to penetrate.

*Make the salsa and sauce.* Toast the chiles in an ungreased skillet over medium heat, turning them for a minute or so until they are aromatic and toasting smelling. Scoop into a small bowl, cover with hot tap water, weight with a plate and soak for about 30 minutes.

In the same skillet, roast the unpeeled garlic, turning from time to time until blackened in spots and soft, about 15 minutes. Cool and peel.

Roast the tomatillos on a rimmed baking sheet about 4 inches below a broiler until blackened and blistered on one side, about 5 minutes, then flip and roast the other side. Cool.

Drain the chiles and scoop into a blender jar with the garlic and tomatillos (and all their juice). Blend to a coarse puree. Add enough water to give the salsa an easily spoonable consistency, then taste and season with salt. A little sugar can help to bring out the chiles’ natural fruitiness. Pour about 2/3 of the salsa into a serving dish. Pour the remainder into a small saucepan and add the orange juice and chicken broth. Bring to a simmer. Taste and add a little more salt and sugar if you think the sauce needs it. Keep warm on the side of the grill.

*Grilling.* Heat a gas grill on medium-high to high or light a charcoal fire and let it burn until the coals are covered with gray ash and very hot. Brush or spray the onion and pineapple slices on both sides with oil. Grill, turning regularly for several minutes, until they are richly colored and softening—you want them to still have nice texture. Cut the core out of the pineapple, then chop the onions and the tender pineapple into small pieces. Keep warm on the side of the grill.

Grill the meat on the very hot grill, cooking it only on one side to duplicate the delicious crusty char every one associates with tacos al pastor. When the meat is cooked, which should take about a minute, cut it into short thin slices. Serve with the onion and pineapple: I find it easiest to mix the meat together with the onion and pineapple, scoop a portion of the mixture into a warm tortilla, add splash on a little of the warm sauce, then top with a spoonful for morita chile salsa and a sprinkling of cilantro. I think tacos al pastor aficionados will be happy.
