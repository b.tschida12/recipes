---
draft: false
title: "Tamarind Chicken Tacos"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients  
  
8 boneless, skinless chicken thighs (about 2 lb.)  
1 12.5-oz. bottle tamarind soda (such as Jarritos brand)  
1/2 cup soy sauce  
3 medium cloves garlic, crushed  
1 Tbs. ground coriander  
2 tsp. pure chile powder, such as ancho  
Sea salt  
Nutritional Information  
Preparation  
  
Put the chicken in a large bowl and cover with the tamarind soda. Add the soy sauce and garlic and mix well. Refrigerate overnight.  
Prepare a high charcoal or gas grill fire for indirect grilling.  
Remove the chicken from the marinade and pat it dry. In a small bowl, combine the coriander, chile powder, and a pinch of salt. Dust the thighs on both sides with the spice rub. Cook on the hot part of the grill until grill marks form on the bottom, 3 to 5 minutes. Turn and mark the other side, about 2 minutes more. Move the chicken to the cooler side of the grill and grill until cooked through, 7 to 10 minutes. Let the chicken rest on a cutting board for 5 minutes and then cut the meat into strips and serve.  
Make Ahead Tips  
  
You can marinate and refrigerate the chicken the night before. You can also grill the chicken up to 30 minutes before you plan to serve it.
