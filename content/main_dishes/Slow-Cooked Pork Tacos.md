---
draft: false
title: "Slow-Cooked Pork Tacos"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1 boneless pork sirloin (2 lbs) cut in 1” pieces

1 ½ c salsa verde

1 red pepper, chopped

1 onion, chopped

¼ c chopped dried apricots

1 T lime juice

2 garlic cloves, minced

1 t cumin

½ t salt

¼ t pepper

Dash hot pepper sauce

Come all

Cook on high 4-5 hours. Shred pork

Serve with tortillas.
