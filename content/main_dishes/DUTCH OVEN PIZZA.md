---
draft: false
title: "DUTCH OVEN PIZZA"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
## **DUTCH OVEN PIZZA**

PREP TIME: 20 MINUTES COOK TIME: 20 MINUTES

TOTAL TIME: 40 MINUTES SERVINGS: 1 10" OR 12" PIZZA AUTHOR: FRESH OFF THE GRID

Here are the basic instructions for how to make pizza in a Dutch oven. Use whatever combination of cheese, sauce, vegetables, and proteins strike your fancy!

### **INGREDIENTS**

-   8-10 oz pizza dough (8 oz for 10", 10 oz for 12")

-   1 tablespoons each flour + cornmeal

-   Toppings of choice

### **INSTRUCTIONS**

1.  **PREPARE YOUR COALS:** Start by preparing your coals or charcoal briquettes. You’ll need about 30 for a 10” Dutch oven, or 33 for a 12” Dutch oven. Once the coals/briquettes are ready, preheat your Dutch oven to 450. For a 10” oven, place 10 coals under the oven and 20 on the lid. For a 12” oven, place 11 coals under the oven and 22 on the lid.

2.  **PREPARE THE DOUGH:** In the meantime, roll out your dough. Dust a cutting board with flour and using a water bottle or bottle of wine (who brings a rolling pin camping?), roll the dough into a circle. Dust the cornmeal onto a piece of parchment and transfer the dough to the parchment paper. Dock the dough all over with a fork (this will prevent the dough from bubbling up while baking).

3.  **TOP:** Add whatever toppings you wish. See our suggestions here.

4.  **BAKE THE PIZZA:** Carefully remove the Dutch oven from the coals and remove the lid. Place the pizza, parchment paper and all, into the Dutch oven, lay the spacers across the top, cover, and return to the bed of coals. Bake for 15-20 minutes, until the crust is golden.

5.  **SERVE & ENJOY!**

Topping suggestions:

-   Feta + Artichoke Hearts + Kalamata Olives + Sun Dried Tomatoes + Oregano + Olive Oil

-   Mozzarella + Cherry Tomatoes + Pesto

-   Blue Cheese + Bacon + Dates + Arugula + Tomato Sauce

-   Mozzarella + Chicken + Red Onions + Cilantro + BBQ Sauce

-   Manchego + Roasted Red Peppers + Chorizo + Tomato Sauce

-   Mozzarella + Sauteed Bell Peppers, Fennel, and Onions + Crumbled Sausage + Tomato Sauce

-   Goat Cheese + Grilled Peaches + Prosciutto + Fresh Basil + Olive Oil

-   Mozzarella + Sliced Tomatoes + Fresh Basil + Tomato Sauce

-   Ricotta + Prosciutto + Spinach + Tomato Sauce

-   Mozzarella + Pineapple + Ham + Tomato Sauce

-   Monterey Jack + Poblanos + Roasted Corn + Onions + Cilantro + Salsa or el Pato Tomato Sauce
