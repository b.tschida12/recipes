---
draft: false
title: "Asparagus and Two Cheese Quiche with Hash-Brown Crust"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
2 lbs shredded potatoes

1 ½ t salt, divided

¾ t pepper, divided

2 T vegetable oil

3 T unsalted butter, divided

4 medium shallots, thinly sliced

6 large eggs, room temp

1 ¼ c half and half

1 t mustard powder

Pinch of grated nutmeg

1 T finely chopped tarragon

5 oz Fontina cheese, grated (1 ½ c)

4 oz fresh goat cheese, crumbled (¾ c)

½ bunch asparagus (½ lb) ends trimmed

Heat oil and 2 T butter in 10” cast-iron skillet over medium-high heat until butter is melted. Add potatoes and immediately start forming into a crust by pushing potatoes flat against the bottom and sides of pan if they start shrinking, until potatoes are bound together and bottom of crust is starting to brown, about 10 minutes. Remove pan from heat and set aside.

Meanwhile, melt remaining 1 T butter in small skillet over medium heat. Add shallots and saute until translucent, 5-6 minutes; set aside.

Preheat oven to 350.

Whisk eggs, half-and-half, mustard powder, nutmeg, remaining ½ t salt and remaining ¼ t pepper in another bowl. Whisk in tarragon and set aside.

Sprinkle cheeses and sauteed shallots evenly over bottom of crust, then pour in egg mixture. Arrange asparagus decoratively on top.

Bake until quiche is set and crust is well browned, 30-35 minutes. Let cool to room temp before cutting into wedges and serving from pan.

Do Ahead:

Quiche can be made up to 1 day in advance. Cool to room temp, then wrap with plastic and refrigerate. To reheat, bake at 325 until warmed through, 15-20 minutes.
