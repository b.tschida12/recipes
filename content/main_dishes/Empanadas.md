---
draft: false
title: "Empanadas"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Empanadas from Bon Appétit

INGREDIENTS

2 pounds boneless, skinless chicken thighs

2 bay leaves

3 tablespoons olive oil

2 large onions, chopped

1 medium green bell pepper, seeded, chopped

1 medium red bell pepper, seeded, chopped

Kosher salt, freshly ground pepper

3 tablespoons tomato paste

2 tablespoons sweet paprika

1 tablespoon dried oregano

¼ teaspoon cayenne pepper

3 packages (12 each) Puff Pastry Dough for Turnovers/Empanadas (preferably Goya)

Bring chicken, bay leaves, and 2½ cups water to a simmer in a medium pot over medium and cook until chicken is cooked through, 30–40 minutes. Transfer chicken to a plate; discard bay leaves and reserve broth. Let chicken sit until cool enough to handle, then coarsely chop.

Meanwhile, heat oil in a large pot over medium and cook onion and bell peppers, stirring, until tender but not browned, 6–8 minutes; season with salt and black pepper. Stir in tomato paste and cook until brick red, 1–2 minutes. Add paprika, oregano, and cayenne and cook, stirring, until fragrant, about 1 minute. Add reserved broth and chicken along with any accumulated juices to pot. Stir in 4 tsp. salt and ¼ tsp. black pepper. Bring to a simmer and cook, stirring and scraping up any brown bits, until most of the liquid is evaporated, 15–20 minutes; taste and season with salt and black pepper, if needed. Transfer to a medium bowl, cover, and chill at least 3 hours.

Preheat oven to 375°. Let dough sit at room temperature 15 minutes to temper. Remove 6 rounds from package, keeping plastic divider underneath, and arrange on a work surface. Place 2 Tbsp. filling in the center of each round. Brush water around half of outer edge of each round. Using plastic divider to help you, fold round over filling and pinch edges to seal. Using a fork, crimp edges. Remove plastic and transfer empanada to a parchment-lined sheet tray, spacing 1" apart. Repeat with remaining rounds (you’ll get about 12 empanadas on each tray).

Bake empanadas, rotating tray halfway through, until golden brown and slightly darker around the edges, 25–35 minutes.

Do Ahead: Filling can be made 3 days ahead. Keep chilled. Unbaked empanadas can be made 3 months ahead; freeze on sheet tray, then transfer to freezer bags and keep frozen.
