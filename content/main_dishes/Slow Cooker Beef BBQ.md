---
draft: false
title: "Slow Cooker Beef BBQ"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1 boneless beef sirloin tip roast (4 lbs) 16 sandwiches

1 (5 ½ oz) can spicy hot V8 juice

½ c water

¼ c vinegar

¼ c ketchup

2 T Worcestershire sauce

½ c brown sugar

1 t salt

1 t ground mustard

1 t paprika

¼ t chili powder

⅛ t pepper

Cut roast in half. Put in slow cooker with all ingredients.

Cook on low 8-10 hours.

Shred beef. Serve on buns.
