---
draft: false
title: "Beef Bourguignon (Julia Child)"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**Ingredients 4/12 - 5 hours**

One 6-ounce piece of chunk bacon

3 1/2 T olive oil

3 pounds lean stewing beef, cut into 2-inch cubes

1 carrot, sliced

1 onion, sliced

Salt and pepper

2 T flour

3 C red wine, young and full-bodied (like Beaujolais, Cotes du Rhone or Burgundy)

2 1/2 to 3 1/2 cups brown beef stock

1 T tomato paste

2 cloves garlic, mashed

1/2 t thyme

1 bay leaf, crumbled

18 to 24 white onions, small

3 1/2 T butter

Herb bouquet (4 parsley sprigs, one-half bay leaf, one-quarter teaspoon thyme, tied in cheesecloth)

1 pound fresh mushrooms, quartered

**Cooking Directions**

Remove bacon rind and cut into lardons (sticks 1/4-inch thick and 1 1/2 inches long). Simmer rind and lardons for 10 minutes in 1 1/2 quarts water. Drain and dry.

*Preheat oven to 450 degrees.*

Sauté lardons in 1 tablespoon of the olive oil in a flameproof casserole over moderate heat for 2 to 3 minutes to brown lightly. Remove to a side dish with a slotted spoon.

Dry beef in paper towels; it will not brown if it is damp. Heat fat in casserole until almost smoking. Add beef, a few pieces at a time, and sauté until nicely browned on all sides. Add it to the lardons.

In the same fat, brown the sliced vegetables. Pour out the excess fat.

Return the beef and bacon to the casserole and toss with 1/2 teaspoon salt and 1/4 teaspoon pepper.

Then sprinkle on the flour and toss again to coat the beef lightly. Set casserole uncovered in middle position of preheated oven for 4 minutes.

Toss the meat again and return to oven for 4 minutes (this browns the flour and covers the meat with a light crust).Remove casserole and turn oven down to *325 degrees.*

Stir in wine and 2 to 3 cups stock, just enough so that the meat is barely covered.

Add the tomato paste, garlic, herbs and bacon rind. Bring to a simmer on top of the stove.

**PREPARATION**

Cover casserole and set in lower third of oven. Regulate heat so that liquid simmers very slowly for **3 to 4 hours**. The meat is done when a fork pierces it easily.

While the beef is cooking, prepare the onions and mushrooms. Heat 1 1/2 tablespoons butter with one and one-half tablespoons of the oil until bubbling in a skillet. Add onions and sauté over moderate heat for about **10 minutes,** rolling them so they will brown as evenly as possible. Be careful not to break their skins. You cannot expect them to brown uniformly. Add 1/2 cup of the stock, salt and pepper to taste and the herb bouquet. Cover and simmer slowly for **40 to 50 minutes** until the onions are perfectly tender but hold their shape, and the liquid has evaporated. Remove herb bouquet and set onions aside. Wipe out the skillet and heat remaining oil and butter over high heat. As soon as you see butter has begun to subside, indicating it is hot enough, add mushrooms. Toss and shake pan for **4 to 5 minutes**. As soon as they have begun to brown lightly, remove from heat.

When the meat is tender, pour the contents of the casserole into a sieve set over a saucepan. Wash out the casserole and return the beef and lardons to it. Distribute the cooked onions and mushrooms on top. Skim fat off sauce in saucepan. Simmer sauce for a minute or 2, skimming off additional fat as it rises. You should have about 2 1/2 cups of sauce thick enough to coat a spoon lightly. If too thin, boil it down rapidly. If too thick, mix in a few tablespoons stock. Taste carefully for seasoning. Pour sauce over meat and vegetables. Cover and simmer 2 to 3 minutes, basting the meat and vegetables with the sauce several times. Serve in casserole, or arrange stew on a platter surrounded with potatoes, noodles or rice, and decorated with parsley.
