---
draft: false
title: "Apple muffins"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1 egg

1 c milk

¼ c vegetable oil

2 c flour

¼ c sugar

3 t baking powder

½ t salt

3 t ground cinnamon

2 ½ med apples, cored, peeled and coarsely grated

Preheat oven to 350F Butter 12 muffin cups, or place papers in each, set aside

Combine egg, milk, and oil in a medium bowl by hand with a spoon or fork. In a large bowl, combine the flour, sugar, baking powder, salt and cinnamon with a fork

Make a well in the center of the dry ingredients, and pour the wet into the dry. Add the grated apples, and then mix just until all ingredients are moistened.

Spoon the batter equally among prepared muffin cups. Bake for 15-20 minutes, or until a toothpick inserted in the center comes out clean
