---
draft: false
title: "Granola with egg white"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
INGREDIENTS

1 large egg white, lightly beaten

3 cups old-fashioned oats

1½ cups chopped nuts (such as almonds, pecans, pistachios, or walnuts)

1½ cups coconut shavings

½ cup maple syrup

¼ cup olive oil or warmed coconut oil

¼ cup sesame seeds

2 tablespoons (packed) light brown sugar

kosher salt to taste

½ teaspoon ground cinnamon

1 cup dried cherries or cranberries

RECIPE PREPARATION

Preheat oven to 300°. Toss egg white, oats, nuts, coconut shavings, agave syrup, oil, sesame seeds, brown sugar, salt, and cinnamon in a large bowl. Spread out on a rimmed baking sheet.

Bake granola, stirring every 10 minutes, until golden brown and dry, 40–45 minutes. Let cool on baking sheet (it will crisp as it cools). Mix in cherries.

Do Ahead: Granola can be made 2 weeks ahead. Store airtight at room temperature.

Customize It

Swap agave syrup or honey for the agave to get a subtle flavor shift.

If chocolate chips mean your kid will love it, throw 'em in at the end.
