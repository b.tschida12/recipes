---
draft: false
title: "Biscuits and Gravy"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**BISCUITS**

Prep Time 15 minutes. Total Time 30 minutes

Servings 12 to 15 biscuits

<u>Ingredients</u>

3 cups all-purpose flour plus more for sprinkling

3 tablespoons sugar

1/2 teaspoon salt

1 heaping tablespoon baking powder

1/2 teaspoon cream of tartar

3/4 cup chilled butter thinly sliced

1 large egg lightly beaten

1 ¼ cups buttermilk

<u>Instructions</u>

Preheat the oven to 350°.. Lightly butter a 12 inch dutch oven, cast iron skillet or baking pan

In a large mixing bowl combine the flour, salt, sugar, baking powder and cream of tartar.

With a pastry cutter or fork, cut the butter into the flour mixture until the butter chunks break down slightly. You don’t want to work it to a cracker crumb consistency, but leave some larger butter flakes.

In a small bowl combine the egg and buttermilk. Stir into the flour mixture just until combined. The dough will be a little sticky. You should be able to see the butter pieces in the dough.

Turn the dough out onto a generously floured surface. Flour your hands and knead the dough 10 to 15 times. If the dough is still tacky, knead in a little more flour to form a soft dough.

Pat or roll the dough out to about ¾ to 1-inch thick. Cut with a biscuit cutter.

Place the biscuits in the skillet, dutch oven or baking sheet. Bake for 10 to 15 minutes or until a light golden brown on top and bottom. Brush the tops with melted butter and serve immediately.

**GRAVY**

Prep Time 10 minutes. Total Time 10 minutes

Servings 2 cups

<u>Ingredients</u>

1/2 cup meat grease sausage, bacon, or other meat grease

5 tablespoons all-purpose flour

1 1/2 to 2 cups milk warmed

Salt and black pepper to taste

<u>Instructions</u>

Heat the grease over medium heat in a large cast iron skillet.

Sift in the flour and let it come to a boil for 2 minutes, stirring and mashing down constantly with a flat spatula.

Slowly stir in 11/2 cups of the milk and bring back to a light boil. Continue stirring until the mixture is smooth and reaches the desired consistency, about 2 minutes. You can add more milk or water to thin the gravy, if necessary.

Season with salt and pepper to taste. Serve hot over biscuits.
