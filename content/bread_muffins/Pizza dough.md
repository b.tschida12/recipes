---
draft: false
title: "Pizza dough"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Yield: Yields 2 lb. dough, enough for 8 cracker-thin crusts, 4 Neapolitan-style crusts, or 1 Sicilian-style crust plus 2 cracker-thin crusts or 1 Neapolitan-style crust

Olive oil keeps this dough tender, and a little sugar encourages browning and boosts the flavor. You can double or triple this recipe.

Ingredients

1-3/4 cups lukewarm water (about 100°F)

1 Tbs. extra-virgin olive oil

2 tsp. kosher salt

1-1/2 tsp. dry yeast (fast-rising or active dry)

1-1/2 tsp. granulated sugar

19 oz. (4-1/4 cups) unbleached all-purpose flour

Preparation

Pour the water into a 3-quart bowl or large, lidded plastic food container. Using a wooden spoon, mix in the oil, salt, yeast, and sugar. Don’t worry about dissolving all of the ingredients; just stir well to combine. Don’t bother “proofing” the yeast, either—it shouldn’t fail if used before its expiration date.

Add the flour and mix until uniformly moist. The dough will be quite wet; no kneading is necessary.

Loosely cover the bowl with plastic wrap or partially cover the plastic container (leave the lid open a crack to let gases escape). Let the dough rise for 2 hours at room temperature. The dough will fully expand and may even begin to collapse within this time.

Do not punch down this dough—it will collapse on its own and later shrink while it chills in the refrigerator; it will never regain its height, and that’s OK.

Refrigerate the dough, loosely covered, for at least 3 hours before using.

Make Ahead Tips

The dough will keep in the refrigerator for up to 2 weeks. After 2 days, tightly cover the dough in its bowl with plastic wrap to keep the surface of the dough from drying out. You can also freeze the dough in well-wrapped 1/2-lb. balls for up to 3 weeks. Thaw overnight in the refrigerator before using.

Whole-wheat dough variation: Substitute 5 oz. (1 cup) whole-wheat flour for the same weight of unbleached all-purpose flour, and add 2 Tbs. additional water.

Cornmeal dough variation: Substitute 5 oz. (1 cup) stone-ground yellow cornmeal for the same weight of unbleached all-purpose flour, and add 1 Tbs. additional olive oil.
