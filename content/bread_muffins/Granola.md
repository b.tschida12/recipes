---
draft: false
title: "Granola"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Granola

Recipe courtesy of Alton Brown

Total:1 hr 25 min

Prep:10 min

Cook:1 hr 15 min

Yield:6 servings

Ingredients

3 cups rolled oats

1 cup slivered almonds

1 cup cashews

3/4 cup shredded sweet coconut

1/4 cup plus 2 tablespoons dark brown sugar

1/4 cup plus 2 tablespoons maple syrup

1/4 cup vegetable oil

3/4 teaspoon salt

1 cup raisins

Directions

Preheat oven to 250 degrees F.

In a large bowl, combine the oats, nuts, coconut, and brown sugar.

In a separate bowl, combine maple syrup, oil, and salt. Combine both mixtures and pour onto 2 sheet pans. Cook for 1 hour and 15 minutes, stirring every 15 minutes to achieve an even color.

Remove from oven and transfer into a large bowl. Add raisins and mix until evenly distributed.
