---
draft: false
title: "Buttermilk biscuits (BA)"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients

MAKES 16 BISCUITS

2 T sugar

2 T baking powder

1 T kosher salt

2 t baking soda

4 cups all-purpose flour, plus more for surface

¾ cup (1½ sticks) unsalted butter, frozen 30 minutes

2 large egg yolks

1⅓ cups buttermilk

Preparation

Step 1

Heat oven to 375°. Whisk sugar, baking powder, salt, baking soda, and 4 cups flour in a large bowl. Using the large holes of a box grater, grate butter into dry ingredients, tossing to coat as you go.

Step 2

Whisk egg yolks and buttermilk in a medium bowl. Using a fork, mix buttermilk mixture into dry ingredients, then gently knead a few times just until a shaggy dough comes together.

Step 3

Transfer dough to a lightly floured surface and pat out until 1” thick; quarter dough and stack pieces on top of one another and press down to adhere (this will create flaky layers). Roll out dough until 1” thick. Using a 2”-diameter biscuit cutter, cut out biscuits, rerolling scraps as needed.

Step 4

Place biscuits on a parchment-lined baking sheet. Bake until golden brown on the tops and bottoms, 20–25 minutes (a few minutes longer if baking frozen).

DO AHEAD: Dough can be made 1 month ahead. Freeze biscuits on a baking sheet, then transfer to resealable plastic bags. Keep frozen.

From Bon Appétit
