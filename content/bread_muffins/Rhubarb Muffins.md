---
draft: false
title: "Rhubarb Muffins"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1-¼ c brown sugar

½ c salad oil

1 egg, bearen

2 t vanilla

1 c buttermilk

1-½ c diced rhubarb

½ c chopped nuts

2-½ c flour

1 t baking soda

1 t baking powder

½ t salt

1 T butter, melted

⅓ c sugar

1 t cinnamon

Combine brown sugar, oil, egg, vanilla, and buttermilk in mixing bowl; mix well. Stir in rhubarb and nuts.

Stir together flour, baking soda, baking powder, and salt.

Stir flour mixture into rhubarb mixture until just blended.

Fill muffin cups2/3 full.

Combine butter, sugar, and cinnamon together then sprinkle over filled cups

Bake at 400° for 20-25 minutes or until delicately browned.

Yield: 20 muffins or 12 regular and 24 mini muffins
