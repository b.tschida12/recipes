---
draft: false
title: "Best Ever Blueberry Muffins"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1 egg

½ c milk

¼ c oil

1 ½ c flour

¾ c sugar

2 t baking powder

½ t salt

½ c fresh or frozen blueberries

Blend above until just moistened.

Bake at 400 degrees for 20-25 minutes.

Makes 12 muffins

Recipe: Sherril (Fogle) Rieger (Cookbook: Woodrow School)
