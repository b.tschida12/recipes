---
draft: false
title: "Chai Banana Bread"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**Dry Mix:**

1 ½ c flour

⅓ c walnuts, chopped

1 t baking soda

1 ½ t ground cinnamon

1 ½ t ground ginger

¼ t ground cardamom

¼ t gorund cloves

¼ t ground allspice

¼ t ground pepper

½ t salt

**Wet Mix:**

⅓ c unsalted butter, melted

½ c light brown sugar

¼ c sugar

3 ripe bananas, smashed

1 large egg

1 t vanilla

**Frosting (optional)**

½ c unsalted butter, room temp

4 oz cream cheese, room temp

2 c powdered sugar

1 t vanilla

Pinch of salt

**DIRECTIONS:**

1.  Preheat oven to 350 degrees. Butter and flour 8 ½ x 4 ½ inch loaf pan, or mini loaf pans.

2.  In a medium bowl, combine dry mix ingredients. In a large bowl, whisk together the melted butter and sugars. Add the egg, then mashed banana and vanilla; whisk until combined.

3.  Add the dry mix to the wet mix, stirring just until combined. Pour the batter into the prepared loaf pan.

4.  Bake for 50 minutes to 1 hour, or until the center has risen, edges are golden brown, and a skewer inserted into the center comes out clean. Allow the bread to cool for 10 minutes in the pan, then invert the loaf and cool on a rack.
