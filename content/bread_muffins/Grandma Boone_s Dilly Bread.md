---
draft: false
title: "Grandma Boone_s Dilly Bread"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Soften 1 pkt yeast in ¼ c warm water

Combine in mixing bowl:

1 c creamed cottage cheese, warm

2 T sugar

1 T minced onions

1 T butter

2 t dill seed

1 t salt

¼ t soda

1 unbeaten egg

Add 2 ½ c flour. Mix well.

Cover and sit it in warm place about an hour.

Punch down and put it in well greased pan.

Let raise for about 30 minutes.

Bake 350 degrees for 40-50 minutes.

So good buttered and warm!

Recipe: Ella Boone (Woodrow School Cookbook)
