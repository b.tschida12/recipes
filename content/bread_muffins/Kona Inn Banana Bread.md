---
draft: false
title: "Kona Inn Banana Bread"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
2-½ c flour

1t salt

2 t baking soda

½ c Crisco

½ c butter

1 c sugar

½ c brown sugar

2 c ripe mashed bananas

(About 6)

4 eggs, slightly beaten

1 t vanilla

1 c toasted coarsely chopped pecans

Preheat oven 350

Grease and flour 2 8x4x3 inch loaf pans

Stir together flour, salt, and baking soda in a small bowl

In a larger bowl, mix together Crisco, butter, sugar, mashed bananas, vanilla, and eggs.

Add the combined ingredients, then toasted pecans and stir only until batter is thoroughly blended.

Pour into prepared pans and bake 50-60 minutes, or until a skewer inserted in the center of the loaves comes out clean, or only with a few moist crumbs.

Remove from oven, let cool in the pan for 5 minutes, then turn out on a rack and cool completely.

Total time: 1 hour 20 minutes

Prep 20 minutes

Cook 1 hour
