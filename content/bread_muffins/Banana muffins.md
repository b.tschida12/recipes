---
draft: false
title: "Banana muffins"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**Ingredients**  
  
2/3 cup sugar  
1/2 cup vegetable oil  
2 eggs  
2/3 cup mashed very ripe bananas (2 small)  
1 teaspoon vanilla  
1 2/3 cups Gold Medal™ all-purpose flour  
1 teaspoon baking soda  
1/2 teaspoon salt  
1/2 teaspoon ground cinnamon  
Topping  
  
1/4 cup sugar  
1/2 teaspoon ground cinnamon  
1/4 cup butter or margarine, melted

**Directions**  
  
Heat oven to 375°F. Grease bottoms only of 12 regular-size muffin cups with shortening or spray, or line with paper baking cups.  
In medium bowl, beat 2/3 cup sugar, the oil and eggs with wire whisk. Stir in bananas and vanilla. Stir in remaining muffin ingredients just until moistened. Divide batter evenly among muffin cups.

Bake 17 to 21 minutes or until toothpick inserted in center comes out clean. Immediately remove from pan to cooling rack.

In small bowl, mix 1/4 cup sugar and 1/2 teaspoon cinnamon. Dip muffin tops into melted butter, then into cinnamon-sugar. Serve warm.

*Betty Crocker*
