---
draft: false
title: "My Fruitcake"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Beat together:

2 c sugar

2 c applesauce

3 eggs

Add:

1 c shortening or margarine

2 8 oz cartons candied cherries

2 cartons candied pineapple

1 lb each white and dark raisins (cook 15 minutes)

1 lb dates

1 c prunes

1 c strawberry jam

1 c each walnuts and pecans

1 t soda

2 heaping t baking powder

1 t each salt, cinnamon, nutmeg

2 t vanilla

2 T rum or rum flavoring

3 c flour

Line greased pans with brown paper and grease. Pour mixture in pans and bake 325 degrees for 1 hour or until done.

Makes 5 loaves.

Cool and wrap in foil and store for 3-4 weeks.

Recipe: Ella Boone (Woodrow School Cookbook)
