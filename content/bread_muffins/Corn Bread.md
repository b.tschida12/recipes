---
draft: false
title: "Corn Bread"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
2 c flour

1 c yellow cornmeal

¾ c sugar

½ t salt

1 T baking powder

2 eggs

1-½ c milk

1-¼ T vegetable oil

¼ c melted butter

Preheat oven 350°

Lightly oil 11x7 pan or use muffin tin

Sift flour, cornmeal, sugar, salt and baking powder together

In separate bowl, mix together eggs, milk, and oil

Pour wet ingredients into dry, then add melted butter. Stir until just mixed

Bake 45 minutes-1 hour. 25-30 minutes for muffins
