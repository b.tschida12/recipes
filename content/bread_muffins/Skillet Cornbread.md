---
draft: false
title: "Skillet Cornbread"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients. 8 SERVINGS

2 cups coarse-grind cornmeal

2 tablespoons sugar (optional)

1 teaspoon kosher salt

½ teaspoon baking powder

½ teaspoon baking soda

1 large egg

1½ cups buttermilk

1 tablespoon lard or vegetable oil

½ cup (1 stick) unsalted butter, room temperature

2 tablespoons finely chopped chives

Flaky sea salt (for serving)

SPECIAL EQUIPMENT

A 10-inch cast-iron skillet

Preparation

Step 1

Place a rack in middle of oven and set skillet on rack; preheat to 450°.

Step 2

Meanwhile, whisk cornmeal, sugar (if using), kosher salt, baking powder, and baking soda in a large bowl to combine. Whisk egg and buttermilk in a medium bowl; add to dry ingredients and mix to incorporate.

Step 3

Carefully remove skillet from oven, add lard, and swirl in pan to coat. Scrape batter into skillet (it should sizzle around edges on contact). Bake cornbread until top is golden brown and center is firm, 15–20 minutes. Let cool 15 minutes before serving.

Step 4

Spread butter over cornbread, top with chives, and sprinkle with sea salt.
