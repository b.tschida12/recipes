---
draft: false
title: "Sweet Corn Tamalito"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**Ingredients**

¼ c butter, softened

¼ c masa harina

⅓ c sugar

½ c water

2 c corn kernels, fresh or frozen & thawed

½ c cornmeal

1 t baking powder

½ t salt

2 T + 1 t milk or heavy cream

Mix butter, masa harina & sugar until light & fluffy, 1 minute

In a blender, mix water and 1 cup corn kernels until smooth.

Mix blended corn together with butter mixture, then add remaining ingredients
