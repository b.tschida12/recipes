---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Crispy Potato Skins"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings: 4-6
prep_time: 10 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 100 # in minutes
calories: # in kcal
---

## Ingredients
- 6 small to medium sized russet baking potatoes (3 lbs)
- Olive oil
- Canola oil
- Kosher salt
- Fresh ground pepper
- 6 strips of bacon
- 4 oz grated cheddar cheese
- ½ c sour cream
- 2 green onions, thinly sliced

## Directions
1. Scrub the potatoes clean then rub with olive oil. bake by either: microwave on high 5 minutes per potato or in 400 degree oven for about an hour
2. While potatoes are cooking, cook bacon until crisp. Drain. Let cool. Crumble
3. Remove potatoes from the oven, let cool enough to handle. Cut in half horizontally. Use a spoon to scoop out insides, reserving scooped potatoes for another use, leaving ¼” potato on skin.
4. Increase oven temperature to 450. Brush or rub canola oil all over skins, outside and in. Sprinkle with salt. Place on baking rack in roasting pan. Cook for 10 minutes on one side, then flip and cook for another 10 minutes Remove from oven and let cool enough to handle
5. Arrange potato skins on rack, sprinkle insides with pepper, cheese and crumbled bacon. Return to oven. Broil for additional 2 minutes, or until cheese is bubbly. Remove from oven. Use tongs to place skins on serving plate. Add a dollop of sour cream to each skin, sprinkle with green onions. Serve immediately.
