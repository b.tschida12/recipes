---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "FRANK_S REDHOT BUFFALO CHICKEN DIP"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings:
prep_time: 5 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 20 # in minutes
calories: # in kcal
---

## Ingredients

- 2 cups shredded cooked chicken 
- 1 (8 oz. pkg.) cream cheese, softened
- 1/2 cup FRANK'S RedHot® Original Cayenne Pepper Sauce or Buffalo Wings Sauce
- 1/2 cup Hidden Valley® Original Ranch® Dressing
- 1/2 cup crumbled bleu cheese or your favorite shredded cheese

## Directions

1. Preheat oven to 350°F. 
2. Combine all ingredients and spoon into shallow 1-quart baking dish.
3. Bake 20 minutes or until mixture is heated through; stir. 
4. Garnish with chopped green onions if desired. Serve with crackers and/or vegetables.

#### Tips

- Reduced Calorie Recipe: Substitute Neufchatel cheese for the cream cheese, and low fat options for the salad dressing and blue cheese.
- Microwave Directions: Prepare as above. Place in microwave-safe dish. Microwave, uncovered, on HIGH 5 minutes until hot, stirring halfway through cooking.
- Slow Cooker Method: Combine ingredients as directed above. Place mixture into small slow cooker. Cover and heat on HIGH setting for 1 1/2 hours until hot and bubbly or on LOW setting for 2 1/2 to 3 hours. Stir.
- Tailgating Tip: Prepare dip ahead and place in heavy disposable foil pan. Place pan on grill and heat dip until hot and bubbly.
