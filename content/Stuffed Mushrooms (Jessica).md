---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Stuffed Mushrooms (Jessica)"
author: "Jessica Simpson"
recipe_image:
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 25 # in minutes
calories: # in kcal
---
## INGREDIENTS
- 1 lb. Italian hot sausage
- 1 1/2 teaspoons dried oregano
- ½ cup freshly grated Parmesan cheese
- 1/2 teaspoon Worcestershire sauce
- 2 cloves garlic, minced
- 4 -ounces cream cheese, room temperature
- 1 large egg yolk
- Olive oil
- 24 large (about 2-inch-diameter) mushrooms, stemmed
- 1/3 cup dry white wine

## Directions
1.  Sauté sausage, oregano, and garlic in heavy large skillet over medium-high heat until sausage is cooked through and brown, breaking into small pieces with back of fork, about 7 minutes. Using slotted spoon, transfer sausage mixture to large bowl and cool. Mix in 1/2 cup Parmesan cheese, and Worcestershire sauce, then cream cheese. Season filling with salt and pepper; mix in egg yolk.
2.  Brush 15x10x2-inch glass baking dish with olive oil to coat. Brush cavity of each mushroom cap with white wine; fill with scant 1 tablespoon filling and sprinkle with some of remaining 1/2 cup Parmesan cheese. Arrange mushrooms, filling side up, in prepared dish. (Can be made 1 day ahead. Cover and chill.)
3.  Preheat oven to 350°F. Bake uncovered until mushrooms are tender and filling is brown on top, about 25 minutes.
