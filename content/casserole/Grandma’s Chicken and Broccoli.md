---
draft: false
title: "Grandma’s Chicken and Broccoli"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
2 Lbs broccoli cut in bite size pieces

2 C cooked chicken, cubed

1 can cream of chicken soup

1 C sour cream

¼ c mayonnaise

1 T lemon juice

1 c cheddar cheese, grated

¼ c buttered bread crumbs

Layer broccoli in the bottom of a 1-½ qt baking dish (9x13)

Top with chicken

Combine soup, sour cream, mayo, and lemon juice. Spread over chicken

Top with cheese and bread crumbs

Bake 350° for 25-30 minutes or until bubbly and heated through

12 g carbs
