---
draft: false
title: "Hash Brown Casserole"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1 package (30 ounces) frozen hash brown potatoes, thawed  
10 tablespoons melted butter,​ divided  
1/2 teaspoon salt  
1/2 teaspoon ground black pepper  
8 ounces sour cream  
1 can (10 3/4 ounces) condensed cream of celery soup  
1 can (10 3/4 ounces) condensed cream of chicken soup  
1/2 cup finely chopped green onion  
8 ounces shredded Cheddar cheese, divided  
1 cup soft fresh bread crumbs

How to Make It  
Grease a 2- to 2 1/2-quart baking dish. Heat oven to 350 F.  
In a large bowl, combine the thawed potatoes, 8 tablespoons of the melted butter, salt, pepper, sour cream, the cream of celery and cream of chicken soups, onion, and 1 1/2 cups of the shredded cheese.  
Spread the mixture in the prepared baking dish.  
In a small bowl, combine the bread crumbs with the remaining 2 tablespoons of melted butter until well blended.  
Sprinkle the remaining cheese over the casserole, then top with the buttered breadcrumbs.

Bake for 25 to 35 minutes, until hot and lightly browned.  
Serves 10 to 12.  
  
Tips and Variations  
  
Instead of bread crumbs, top the casserole with crumbled French fried onions.  
For a vegetarian casserole, replace the cream of chicken soup with cream of mushroom.
