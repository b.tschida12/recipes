#!/bin/zsh

# Convert *.docx to *.md while extracting any images
# dir=
# echo "Which directory to convert?"
# vared dir

# for dir in *; do
#     for file in $dir/*; do
#         echo "$file"
#     done
# done
test='docx'
set +v
repeat=

# while [ "$repeat" = "" ]; do
    for dir in *; do
        if [[ -d $dir ]]; then
            for FILE in $dir/*; do
                ext=$FILE:t:e
                echo "$FILE"
                echo "$ext"
                if [[ $ext==$test ]]; then
                    break
                else
                
                        vim $FILE
                    echo "Hit enter to continue"
                    read repeat
                fi
            done
        fi
    done
# done
