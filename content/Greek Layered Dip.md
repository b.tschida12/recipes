---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Greek Layered Dip"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings:
prep_time: 20 # in minutes
cook: false# true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
## Ingredients
- Hummus
- Cucumber, diced
- Tomatoes, diced
- Bell peppers, diced
- Onion, diced
- Olives, diced
- Greek yogurt
- Feta, crumbled

## Directions
- Similar to Mexican 7 layer dip
