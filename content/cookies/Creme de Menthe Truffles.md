---
draft: false
title: "Creme de Menthe Truffles"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Yeld: 4-½ dozen

½ c butter, melted

1 c finely chopped pecans

¼ c creme de menthe

4 c powdered sugar

2 c chocolate chips (12 oz)

2 t shortening

In a large bowl, combine butter, pecans & creme de menthe. Gradually beat in powdered sugar. Cover - refrigerate 1 hour

With hands lightly dusted with powdered sugar, shape mixture into 1” balls. Place on waxed paper. Chill 30 minutes or until firm

Melt chocolate chips & shortening in bowl. Microwave until melted. Stir until smooth. Dip balls into chocolate, let excess drip off. Return to waxed paper. Store in fridge.
