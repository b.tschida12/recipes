---
draft: false
title: "Chocolate Crinkle Cookies"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**<u>Chocolate Crinkle Cookies</u>**

½ c oil

4 sq unsweetened chocolate, melted

2 c sugar

4 eggs

2 t vanilla

½ t salt

2 c flour

2 t baking powder

1 c powdered sugar

Combine oil, chocolate, and sugar in large bowl. Blend in 1 egg at a time, until well mixed. Add vanilla. Stir in salt, flour and baking powder. Blend thoroughly. Chill overnight.

Preheat oven 350 degrees. Drop dough by teaspoonfuls into powdered sugar. Shape into balls. Place 2” apart on baking sheet.

Bake 10-12 minutes. Makes 50 cookies
