---
draft: false
title: "Baklava Tartlets"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Yield: 45 tartlets

2 c finely chopped walnuts

¾ c honey

½ c butter, melted

1 t cinnamon

1 t lemon juice

¼ t ground cloves

3 packages frozen mini phyllo tart shells

Mix ingredients together. Spoon 2 t into each tart shell. Refrigerate until serving

-   Better if chilled 1 hour before serving
