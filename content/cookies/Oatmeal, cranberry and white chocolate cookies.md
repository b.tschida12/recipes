---
draft: false
title: "Oatmeal, cranberry and white chocolate cookies"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**<u>Ingredients</u>**

1 cup all purpose flour

1 tsp baking soda

1 tsp cinnamon

1/4 tsp salt

1 cup unsalted butter (melted)

1/2 cup brown sugar

1/4 cup granulated sugar

1 large egg

1 tsp vanilla extract

2 1/2 cups rolled oats

1 cup dried cranberries

1 cup white chocolate chips

**<u>Instructions</u>**

In a medium-size mixing bowl sift together all-purpose flour, baking soda, cinnamon and salt, and put it aside.

In another large mixing bowl put all wet ingredients together (melted butter, brown sugar, granulated sugar, vanilla extract and a large egg). Mix it again.

Add dry ingredients to the mixture in two steps and mix it well until it is smooth.

Add rolled oats and mix it using a spatula. Add cranberries and white chocolate chips and mix again. Cover the dough with plastic wrap and place it in *refrigerator for an hour.*

When the dough is ready, preheat the oven to 350°F and line a baking sheet with parchment paper or silicone baking mat.

Using a 1 1/2 tablespoon ice cream scoop or a regular spoon, scoop the dough and roll it into balls. Place it on a baking sheet with parchment pepper.

Bake the cookies for about 10-12 minutes until it gets beautiful lightly golden color outside. Make sure you don’t over bake them, so they won’t be dry inside.

Nutrition

**Serving Size: 1 cookie**

Calories: 167

Sugar: 12.2

Sodium: 65.6

Fat: 8.5

Saturated Fat: 5.3

Trans Fat: 0

**Carbohydrates**: 21

Fiber: 1.6

Protein: 2.1
