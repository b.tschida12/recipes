---
draft: false
title: "Snickerdoodles"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**<u>Snickerdoodles</u>**

2-¾ c flour

1 t baking soda

½ t salt

½ c shortening

8 T butter (1 stick) softened

1½ c sugar, plus 3 T

2 large eggs

1 T cinnamon

Mix flour, baking soda, and salt into a bowl.

Beat together shortening and butter. Add 1-½ c sugar, continue beating until light and fluffy, about 5 minutes. Add the eggs, 1 at a time, beating well after each addition. Add the flour mixture and blend until smooth.

Mix the 3 T sugar with the cinnamon in a small bowl. Roll the dough by hand into 1-½” balls. Roll the balls in the cinnamon sugar. Flatten the balls into ½” thick disks, spacing them evenly on unlined cookie sheets.

Bake about 12 minutes, or until light brown, but still moist in the center. Cool on a rack.

Makes about 20 cookies
