---
draft: false
title: "Southern Pecan Bars"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**<u>Southern Pecan Bars</u>**

1 c butter, softened

¾ c brown sugar, packed

2-¼ c flour

½ t vanilla

4 large eggs

1 c light corn syrup

⅓ c brown sugar

¼ c butter, melted

3 t flour

1 t vanilla

2 c coarsely chopped pecans

Preheat oven 350 degrees.

Beat 1 c butter at medium speed with mixer until creamy; gradually add ¾ c brown sugar, beating well. Stir in ½ t vanilla. Press into bottom of ungreased 13x9x2 pan. Brush with egg white.

Bake 15 minutes, cool 5 minutes.

Beat eggs in large mixing bowl at medium speed until thick and pale. Combine corn syrup, brown sugar, melted butter, flour and 1 t vanilla, stirring until smooth. Fold in pecans. Spoon mixture over crust.

Bake for 30 minutes. Cool completely. Cut into bars

Makes 48 cookies
