---
draft: false
title: "Peanut butter Bon Bons"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
TOTAL TIME: Prep: 30 min. + chilling

YIELD: 5-1/2 dozen.

Ingredients

5-1/2 cups confectioners' sugar

1-2/3 cups peanut butter

1 cup butter, melted

4 cups semisweet chocolate chips

1 teaspoon shortening

Directions

1\. In a large bowl, beat the sugar, peanut butter and butter until smooth. Shape into 1-in. balls; set aside.

2\. Microwave chocolate chips and shortening on high until melted; stir until smooth. Dip balls in chocolate, allowing excess to drip off. Place on a wire rack over waxed paper; refrigerate for 15 minutes or until firm. Cover and store in the refrigerator.

Nutrition Facts

1 piece: 127 calories, 6g fat (2g saturated fat), 0 cholesterol, 29mg sodium, 18g carbohydrate (16g sugars, 1g fiber), 2g protein.
