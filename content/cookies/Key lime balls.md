---
draft: false
title: "Key lime balls"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1 package (8 ounces) cream cheese, softened

1 can (14-1/2 ounces) sweetened condensed milk

3-1/2 cups graham cracker crumbs

3 tablespoons lime juice

2 teaspoons grated lime peel

1/4 teaspoon salt

1/4 teaspoon ground cinnamon

1/2 cup confectioners' sugar

Half condensed milk. Double lime

In a large bowl, beat the first seven ingredients until blended. Shape into 1-in. balls. Roll cookie balls in confectioners' sugar. Refrigerate 30 minutes or until firm.

Store cookies between pieces of waxed paper in an airtight container in the refrigerator.
