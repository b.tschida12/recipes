---
draft: false
title: "Glazed Lemon Thins"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
2T fresh lemon juice

1 large egg yolk

1-2 t vanilla

¾ c sugar

2 T grated lemon zest

1 ¾ c flour

¼ t baking powder

¼ t salt

12 T (1-½ sticks) cold unsalted butter, chopped

GLAZE

1 T cream cheese, room temp

2 T fresh lemon juice

1-¼ c powdered sugar

In small bowl whisk lemon juice, egg yolk, and vanilla until combined

In a food processor, mix sugar and zest until combined, about30 seconds. Add flour, baking powder, and and salt; pulse until combined. Add butter pieces, process until mixture resembles corn meal. With machine running, slowly pour juice through tube. Process until dough forms a ball.

Transfer dough to a lightly floured counter. Roll into a 2” wide, 10” long log. Wrap tightly in plastic. Refrigerate for at least 2 hours, until firm.

Preheat oven to 325°. Line 2 baking sheets with parchment paper. Unwrap dough, slice into disks between ¼ and ½ inch thick. Place on prepared about 1” apart.

Bake 15 minutes cool 3 minutes cool on wire rack.

For glaze, in medium bowl whisk cream cheese and lemon juice until smooth. Gradually fold in powdered sugar. Frost cookies.
