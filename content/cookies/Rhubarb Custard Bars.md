---
draft: false
title: "Rhubarb Custard Bars"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
TOTAL TIME: Prep: 25 min. Bake: 50 min. + chilling

YIELD: 3 dozen.

Ingredients

2 cups all-purpose flour

1/4 cup sugar

1 cup cold butter

FILLING:

2 cups sugar

7 tablespoons all-purpose flour

1 cup heavy whipping cream

3 large eggs, room temperature, beaten

5 cups finely chopped fresh or frozen rhubarb, thawed and drained

TOPPING:

6 ounces cream cheese, softened

1/2 cup sugar

1/2 teaspoon vanilla extract

1 cup heavy whipping cream, whipped

Directions

1\. In a bowl, combine the flour and sugar; cut in butter until the mixture resembles coarse crumbs. Press into a greased 13x9-in. baking pan. Bake at 350° for 10 minutes.

2\. Meanwhile, for filling, combine sugar and flour in a bowl. Whisk in cream and eggs. Stir in the rhubarb. Pour over crust. Bake at 350° until custard is set, 40-45 minutes. Cool.

3\. For topping, beat cream cheese, sugar and vanilla until smooth; fold in whipped cream. Spread over top. Cover and chill. Cut into bars. Store in the refrigerator.
