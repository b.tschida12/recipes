---
draft: false
title: "Raspberry Bars"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Prep Time: 20 minutes Cook Time: 40 minutes Total Time: 1 hour

Ingredients

2 cups all purpose flour

2 cups rolled oats

1 1/3 cup brown sugar

1/2 teaspoon baking soda

1/2 teaspoon salt, more if you used unsalted butter

1 cup cold butter

1 1/2 cups raspberry jam

1/2 cup fresh raspberries, or sub in an extra scoop of jam

Instructions

Preheat oven to 350˚

in a bowl combine the flour, oats, brown sugar, baking soda, salt. Cut the cold butter in small chunks, and toss with the dry ingredients, then use a pastry blender or two knives to cut the butter in, keep working until it resembles crumbs

Measure out 1 cup and set aside. Press the remaining crumbs in a 9X13 pan

spread the jam over the top of the base layer, sprinkle with the fresh berries, then crumble the remaining mixture over the berries.

Bake for 35-40 minutes or until the top is lightly browned and the bottom crust is set (you can give it a little poke with a knife to test it)

Nutrition Information Yield 24 Serving Size 1

Amount Per Serving Calories 202Total Fat 3gSaturated Fat 1gTrans Fat 0gUnsaturated Fat 2gCholesterol 8mgSodium 147mgCarbohydrates 40gFiber 1gSugar 20gProtein 4g
