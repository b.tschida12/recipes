---
draft: false
title: "Old Fashioned Molasses Chews"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**<u>Old Fashioned Molasses Chews</u>**

¾ c oil

¼ c dark molasses

1 c sugar

2 eggs

2-¾ c flour

1½ t baking soda

1 t cinnamon

1 t ground ginger

¼ t ground cloves

Stir together oil, molasses and 1 c sugar. Add eggs, beat until smooth. Stir together dry ingredients, add to oil mixture. Cover with plastic wrap, refrigerate 1 hour or until next day.

Preheat oven 350 degrees. Roll dough in 1-½” balls. Roll in granulated sugar.

Bake 10-12 minutes.

Makes 30 cookies
