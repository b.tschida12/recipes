---
draft: false
title: "Heath Bar toffee cookies"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients  
  
2 1/2 cups all-purpose flour  
1 teaspoon salt  
1 teaspoon baking soda  
1 cup (two sticks) unsalted butter, softened  
1 1/2 cups granulated sugar  
2 eggs  
1 teaspoon vanilla  
1 1/2 cups chopped Heath Bar pieces (about eight regularly-sized 1.4 ounce bars) — I used my food processor to chop these bars  
Directions  
  
In a large mixing bowl, sift together flour, salt, and baking soda and set aside.  
In a mixer, beat together the butter and sugar. Beat in eggs, one at a time, and add vanilla.  
Alternatively mix in the Heath Bar mixture and the flour mixture, a third at a time, until well blended.  
Cover and chill cookie dough for an hour.  
Preheat oven to 350 degrees, and line cookie sheet with your trusty Silpat (or parchment paper).  
Roll chilled cookie dough into small, two-inch diameter balls. Be sure to leave a bit of space between each ball of dough, as they tend to spread while baking.  
Bake for 10 to 12 minutes, until the edges are just beginning to brown (cookies may appear underbaked).  
Remove from oven and let cool on baking sheet for a couple of minutes, then cool completely on a wire rack.
