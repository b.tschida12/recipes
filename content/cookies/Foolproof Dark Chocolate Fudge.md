---
draft: false
title: "Foolproof Dark Chocolate Fudge"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Foolproof Dark Chocolate Fudge

3 c chocolate chips

1-14 oz can sweetened condensed milk

½ c -1 c chopped nuts

1-½ t vanilla

Dash salt

Microwave chips, milk and salt 3 minutes, or until chips melt, stirring after each 30 seconds. Stir in remaining ingredients. Line 9x9 pan with parchment. Pour into 9x9 pan. Chill.

Optional additions: 2 c mini marshmallows
