---
draft: false
title: "Swedish Gingerbread Cookies"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
⅔ c brown sugar

⅔ c light molasses

1 t ginger

1 t cinnamon

½ t cloves, ground

1 ¼ t baking soda

⅔ c butter

1 egg

5 c flour

Heat sugar, molasses and spices to boiling point. Add baking soda and pour mixture over butter in bowl. Stir until butter melts.

Add egg and sifted flour and blend thoroughly.

Knead, chill.

Roll out and cut with cookie cutters.

Place on greased cookie sheet. Bake at 325 degrees for 8 minutes.

Recipe: The Fogle kitchen (Grace Fogle) (Woodrow School cookbook)
