---
draft: false
title: "Banana-Oatmeal Bars with Chocolate Chunks"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Banana-Oatmeal Bars with Chocolate Chunks

2 c flour

1 c quick-cooking oats

1 T baking powder

¾ t salt

1 c (2 sticks) butter, room temp

1-¼ c sugar

1-¼ c (packed) brown sugar

2 eggs

⅓ c mashed ripe bananas (about 2 large)

2 t vanilla

8 oz chocolate pieces

1 c pecans, toasted, chopped

Preheat oven to 350 degrees. Butter and flour 15x10x1 baking sheet. Blend first 4 ingredients in medium bowl. Beat butter in large bowl until fluffy. Add both sugars and beat until well blended. Add eggs, 1 at a time, beating well after each addition. Beat in bananas, then vanilla. Stir in flour mixture, then chocolate and pecans.

Spread batter in prepared pan. Bake until tester inserted into center comes out clean and top is golden, about 45 minutes. Cool in pan on rack. Cut into 3x2-inch bars and serve.

Makes about 3 dozen bars.
