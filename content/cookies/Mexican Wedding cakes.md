---
draft: false
title: "Mexican Wedding cakes"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**<u>Mexican Wedding cakes</u>**

1 c butter, room temp

½ t vanilla

½ c chopped pecans

1¾ c flour

½ c plus 4 T powdered sugar

Preheat oven 350 degrees.

Cream butter and ½ c powdered sugar in large bowl until very light. Blend in vanilla, flour and pecans. Shape dough into balls, about 1”. Place on baking sheet

Bake for 20 minutes. Cool. Roll in powdered sugar.
