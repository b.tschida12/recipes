---
draft: false
title: "Raspberry Lemon Cheesecake Bars"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
2 c graham cracker crumbs

1 c + 2T sugar, divided

6 T butter, melted

3 c (12oz) raspberries, divided

1 T each zest and juice from 1 lemon

4 pkg (8 oz each) cream cheese, softened

4 eggs

Heat oven 350•

Line 13x9 pan with foil, ends of foil extending over sides. Combine graham crumbs, 2 T sugar and butter; press onto bottom of prepared pan. Bake 10 minutes

Reserve ½ c raspberries and 1 t lemon zest

Beat cream cheese, lemon juice, remaining zest and remaining sugar in large bowl with mixer until blended. Add eggs, 1 at a time, mixing on low speed after each just until blended. Gently stir in remaining raspberries; pour over crust.

Bake 35-40 minutes or until center is almost set. Cool completely

Refrigerate 4 hours. Top with reserved raspberries and lemon zest. Use foil handles to remove cheesecake from pan before cutting into bars

Prep time: 15 minutes

Total time: 6 hours

Makes 18 servings
