---
draft: false
title: "Pecan Tassies"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**<u>Pecan Tassies</u>**

½ c margarine

3 oz cream cheese, softened

1 c flour

\_\_\_\_\_

¾ c brown sugar

1 T butter

1 egg

¾ t vanilla

1½ c pecans, chopped

Blend together margarine, cream cheese and flour in large bowl. Form into 24 small balls, place each ball in mini muffin pan. Press to form crust.

Combine remaining ingredients, mix thoroughly. Place 1 t filling in each cup.

Bake at 350 degrees 15-20 minutes. Watch carefully.

Makes 24 cookies
