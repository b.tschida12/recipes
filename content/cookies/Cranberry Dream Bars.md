---
draft: false
title: "Cranberry Dream Bars"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**<u>Cranberry Dream Bars</u>**

1 c margarine

¾ c sugar

¾ c brown sugar, packed

2 eggs

1 t vanilla

2-¼ c flour

1 t baking powder

1-12 oz package white chocolate chips

6 oz dried cranberries

1 c nuts, chopped

Preheat oven to 350 degrees

Combine margarine and sugars, beat until well blended, then add eggs and vanilla.

Mix in flour, baking powder, then stir in chocolate, cranberries and nuts.

Spread batter in 15x10x1 pan. Bake 20 minutes. Cool. Cut into bars
