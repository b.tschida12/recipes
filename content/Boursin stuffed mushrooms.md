---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Boursin stuffed mushrooms"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings:
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 20 # in minutes
calories: # in kcal
---
## Ingredients

- 2 tbsp. butter
- 1/2 finely chopped onion
- 20 button mushrooms, stemmed and chopped
- 2 cloves garlic, minced
- 1 tbsp. finely chopped parsley, plus more for garnish
- block Boursin
- 1/2 c. breadcrumbs, plus more for sprinkling
- 1/2 c. grated Parmesan
- kosher salt
- Freshly ground black pepper
- Extra-virgin olive oil, for drizzling

## Directions 

1. Preheat oven to 350°. In a large skillet, melt butter. Add onion, mushroom stems, garlic, and parsley.
2. In a small bowl, mix together Boursin, breadcrumbs, grated Parmesan, and mushroom stem mixture and season with salt and pepper.
3. Stuff mushrooms and sprinkle with more breadcrumbs and drizzle with olive oil.
4. Garnish with finely chopped parsley and bake until mushrooms are deeply golden and cooked through, 20 minutes.
5. Serve.
