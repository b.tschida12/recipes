---
draft: false
date: 2021-01-25T20:16:56-05:00
title: "Bacon Wrapped Pineapple"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings:
prep_time: 10 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 25 # in minutes
calories: # in kcal
---

## Ingredients

- Fresh pineapple, cut into 1” chunks
- (Pre-grill OT use fresh)
- 16 oz regular bacon, cut in half
- Drain pineapple and dry well

## Directions
1. 425° 15 minutes, turn, 10 minutes more
