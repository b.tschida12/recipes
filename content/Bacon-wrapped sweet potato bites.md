---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Bacon-wrapped sweet potato bites"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings: 30
prep_time: 25 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 40 # in minutes
calories: # in kcal
---
## Ingredients

- 2 tablespoons butter, melted
- 1/2 teaspoon salt
- 1/2 teaspoon cayenne pepper
- 1/4 teaspoon ground cinnamon
- 2 large sweet potatoes (about 1-3/4 pounds), peeled and cut into 1-inch cubes
- 1 pound bacon strips, halved
- 1/4 cup packed brown sugar
- Maple syrup, warmed

## Directions

1. Preheat oven to 350°. In a large bowl, mix butter and seasonings. Add sweet potatoes and toss to coat.
2. Wrap 1 piece bacon around each sweet potato cube; secure with a toothpick. Sprinkle with brown sugar. Place on a parchment-lined 15x10x1-in. baking pan.
3. Bake until bacon is crisp and sweet potato is tender, 40-45 minutes. Serve with maple syrup.
