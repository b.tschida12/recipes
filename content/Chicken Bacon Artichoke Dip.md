---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Chicken Bacon Artichoke Dip"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings:
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 30 # in minutes
calories: 387.57 # in kcal
---
## Ingredients

- 2 cups chicken cooked and shredded
- 1/2 lb bacon cooked and chopped
- 14 oz can quartered artichoke hearts drained and chopped
- 2 cups packed fresh spinach
- 3 cloves garlic minced
- 1/2 cup mayonnaise
- 1/4 cup milk
- 4 oz cream cheese softened and cut into chunks
- 1 1/2 cups shredded mozzarella cheese divided
- 1 cup shredded Swiss cheese
- 1/4 tsp cayenne pepper
- 1 tsp salt
- 1/2 tsp black pepper

## Instructions

1. Preheat oven to 350 degrees F. Coat a medium-sized baking dish with cooking spray and set aside.
2. In a medium bowl, combine the chicken, bacon, artichoke hearts, spinach, garlic, mayo, milk, cream cheese, 1 cup of the mozzarella cheese, Swiss cheese, cayenne pepper, salt and black pepper. Mix well.
3. Transfer mixture to the prepared baking dish. Top with 1/2 cup shredded mozzarella cheese. 
4. Bake in the preheated oven for 30 minutes. Serve warm with chips or crackers.

## Notes
-   Try stuffing zucchini with mixture
-   Either shred cooked chicken breasts for this recipe or pull the meat from a Rotisserie chicken.
-   Fresh spinach tastes best, but frozen spinach thawed (1 cup) will work great, too.
-   Add a FOURTH cheese by sprinkling Parmesan cheese over the top before baking.
-   Replace all or a portion of the mayo with sour cream, if desired.

#### Nutrition
Calories: 387.57kcal \| Carbohydrates: 8.36g \| Protein: 22.53g \| Fat: 29.46g \| Saturated Fat: 10.79g \| Cholesterol: 82.72mg \| Sodium: 845.5mg \| Fiber: 2.45g \| Sugar: 1.35g
