---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Spinach and artichoke puff pastry pinwheel"
author: "Betty Crocker"
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings:
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 20 # in minutes
calories: # in kcal
---
## Ingredients
- 1 can (14 oz) Progresso™ artichoke hearts, drained, chopped
- 1 box (9 oz) frozen chopped spinach, thawed, squeezed to drain
- ½ cup mayonnaise
- ½ cup grated Parmesan cheese
- 1 teaspoon onion powder
- 1 teaspoon garlic powder
- ½ teaspoon pepper
- 1 package (17.3 oz) frozen puff pastry sheets, thawed

## Steps

1.  In medium bowl, stir together artichoke hearts, spinach, mayonnaise, cheese, onion powder, garlic powder and pepper.
2.  Unfold 1 puff pastry sheet on lightly floured surface. Spread half of the spinach mixture over pastry to within 1/2 inch of edges. Starting with short side, roll up pastry, pressing to seal seam; wrap tightly in plastic wrap. Repeat with remaining pastry and spinach mixture. Freeze 30 minutes.
3.  Heat oven to 400°F. Unwrap pastry; cut each roll into 1/2-inch slices. Place on ungreased cookie sheets.
4.  Bake 20 minutes or until golden brown. Serve warm.

