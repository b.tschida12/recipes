---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Sweet Spicy Nuts"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 18-20 # in minutes
calories: # in kcal
---
## Ingredients
- 1/4 cup packed brown sugar  
- 1/2 teaspoon ground cinnamon  
- 1/4 teaspoon cayenne pepper  
- 1 egg white  
- 1 cup salted cashews  
- 1 cup pecan halves  
- 1 cup dry roasted peanuts  
- 1/2 cup dried cranberries

## Directions
1. In a small bowl, combine the brown sugar, cinnamon and cayenne; set aside. In a large bowl, whisk egg white; add nuts and cranberries. Sprinkle with sugar mixture and toss to coat. Spread in a single layer on a greased baking sheet.  
2. Bake at 300° for 18-20 minutes or until golden brown, stirring once. Cool. Store in an airtight container. Yield: 3-1/2 cups.
