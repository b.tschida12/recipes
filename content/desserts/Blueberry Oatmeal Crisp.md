---
draft: false
title: "Blueberry Oatmeal Crisp"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
INGREDIENTS

3 pints blueberries

1/2 cup sugar

juice and finely grated zest of 1 lemon

3 tablespoons flour

INSTRUCTIONS

Mix the berries, sugar, lemon juice and zest, and flour in a large bowl. Preheat the oven to 400 degrees F, and butter a 13x9-inch glass or ceramic baking dish.

TOPPING

INGREDIENTS

1 cup unbleached all-purpose flour

1 cup packed light-brown sugar

2/3 cup rolled oats

1/2 teaspoon cinnamon

1/4 teaspoon salt

1/2 cup (1 stick) plus 2 tablespoons cold, unsalted butter, cut into small pieces

INSTRUCTIONS

For topping: Combine the flour, brown sugar, oats, cinnamon, and salt in the bowl of a food processor. Add the butter, and pulse the machine repeatedly, in 2- to 3-second bursts, until the mixture is clumpy, like damp crumbs. Transfer the berries to the baking dish, and spread the crumbs evenly over the fruit. Bake for 30 minutes, until bubbly hot. Serve at any temperature, although it is best to let it cool at least 10 minutes.

YIELD:

8 to 10 servings
