---
draft: false
title: "Rhubarb Crisp"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
INGREDIENTS

2 pounds rhubarb, sliced crosswise 3/4 inch thick (about 3 c)

1 cup granulated sugar

½ t salt

1 T instant tapioca

½ c flour

1/2 cup (1 stick) cold unsalted butter, cut into pieces

1/2 cup packed light-brown sugar

1 cup rolled oats

1/2 teaspoon ground cinnamon

Ice cream, for serving (optional)

DIRECTIONS

1\. Preheat oven to 400 degrees. In a 9-by-13-inch baking dish, combine rhubarb, sugar, salt, and tapioca; set aside.

2\. In the bowl of a food processor, combine 1/2 cup flour and the butter. Pulse until the butter pieces are pea-size. Add brown sugar, oats, and cinnamon. Pulse to combine. Sprinkle over rhubarb.

3\. Bake until rhubarb is tender and topping is golden, 35 to 45 minutes. Serve warm with ice cream, if desired.
