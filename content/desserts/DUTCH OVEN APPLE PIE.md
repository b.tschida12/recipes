---
draft: false
title: "DUTCH OVEN APPLE PIE"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
## **DUTCH OVEN APPLE PIE**

PREP TIME: 45 MINUTES COOK TIME: 1 HOUR PREP TIME AT CAMP: 20 MINUTES

TOTAL TIME: 2 HOURS 5 MINUTES SERVINGS: 6 SERVINGS AUTHOR: FRESH OFF THE GRID

### **INGREDIENTS**

#### **CRUST**

-   1 cup AP flour

-   1 tablespoon granulated sugar

-   ½ teaspoon salt

-   1 stick cold butter (1/2 cup)

-   5 tablespoons ice cold water

#### **FILLING**

-   4 apples

-   ½ cup granulated sugar

-   1 teaspoon ground cinnamon

-   2 tablespoons cornstarch

-   1 egg

### **INSTRUCTIONS**

#### **AT HOME: MAKE THE CRUST**

1.  Mix the flour, sugar, and salt together in a medium bowl. Cut the cold butter into cubes and add to the dry ingredients. Using your fingers or a pastry cutter, smear the butter into the dry ingredients until a crumbly dough forms. Incorporate the ice water into the dough, 1 tablespoon at a time, mixing with your hand, until it comes together into a moist-but-not-sticky ball.

2.  Transfer the dough to a floured work surface and flatten the dough into a 6” disc. Cut the disc into four pieces. Stack the pieces on top of each other and then press down to combine them back into a single ball again. Repeat this process 2-3 times.

3.  Form into a final disc, then tightly wrap or place it into an airtight container and place in the fridge. When packing for your camping trip, this dough should go directly from your refrigerator into your pre-chilled cooler.

#### **AT CAMP: ASSEMBLE THE PIE**

1.  Start your charcoals in a chimney starter.

2.  **Make the filling:** peel, core, and slice apples into ⅛” - ¼” slices. Place the slices into a large mixing bowl or pot. Add in the sugar, cinnamon, and cornstarch and mix so the apples are evenly coated. Set aside.

3.  **Prep the Dutch oven:** Using parchment paper, create two “straps” and set them in the bottom of a 10" (4qt) Dutch oven in an “X”. Line the oven with a circle of parchment paper set over the straps.

4.  **Roll Out the Crust:** Lightly flour a large cutting board and retrieve the dough from the cooler/refrigerator. Cut ⅔ of the disc and return the extra ⅓ to the refrigerator. Form the dough into a disc in your hands, allowing it to warm up ever so slightly.

5.  Place the disc on the cutting board and using a rolling pin, hydroflask, or wine bottle whack the disc a few times, rotate a quarter turn and whack a few times again. Do a full rotation like this until the disc is compressed, circular, and expanded slightly. Then start rolling the dough, applying the most pressure in the center and let up as your roll towards the edges. After each roll rotate the dough a little bit, so you’re evenly spreading it.

6.  Once the dough is about 2” larger than the bottom of your dutch oven (or roughly the size of parchment paper you cut out), place your rolling pin at one end, hold the edge to the pin, and slowly roll up your dough (so it is wrapped around the pin). Then unroll it into the Dutch oven.

7.  **Assemble the Pie:** Using a slotted spoon or pair of tongs, transfer the apple slices into the Dutch oven, making sure they are in as even of a layer as possible.

8.  Retrieve the remaining ⅓ portion of dough from the refrigerator/cooler. Roll it out the same as before into a \~10” circle. This will be the top of the crust. You can use the Dutch oven lid as reference. Then, roll this dough up on the rolling pin and unroll it onto the top of the pie.

9.  Pinch the top dough and bottom dough together to form a seal. You can wet your hands a little if the dough isn’t sticking. You want a good seal all the way around so to avoid a blow out and achieve a flaky crust.

10. In a small bowl, bear an egg until completely mixed. Brush the egg over the top of the pie. Use a knife to cut 4 slices in to your dough to allow steam to escape.

11. **Cook the Pie:** Dump a layer of coals on the ground and place your Dutch Oven on top of it. Place two flat metal skewers across the top of the Dutch Oven and then place your Dutch Oven lid on top. Place the remaining coals on the Dutch Oven lid, favoring the outside rim over the center.

12. Immediately start a new batch of coals. Monitor the coal temperature periodically by hovering your hand over them. If you want to be able to hold your hand there for a 2-3 seconds but not more. Rotate the oven over the bottom coals and rotate the lid that contain the top coals to even out any unintentional hot spots. If you feel the heat start to drop on either the top or bottom, replenish with new coals.

13. **Cook for about an hour,** until the top of the pie looks golden brown, then remove the Dutch Oven from the heat.

14. Allow the pie to cool to “room” temperature before serving. Using the parchment straps, lift the pie out of the oven. Once cooled, cut into 6-8 slices and serve.
