---
draft: false
title: "Egg Nog Creme Brulee"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
YIELDS: 12 servings.

PREP TIME: 5 mins. COOK TIME: 35 mins TOTAL TIME: 40 mins

Ingredients

3 1/2 c. Heavy Cream

8 Large Egg Yolks

1 c. Sugar, Divided

1/2 c. Bourbon Or Spiced Rum

1 tbsp. Vanilla Extract

1 1/2 tsp. Ground Nutmeg

1 pinch Salt

Directions

Preheat oven to 325ºF. Place a 9 X 13 inch baking dish, filled halfway with water, on the bottom rack of the oven to create steam. Set the second oven rack to the middle position. Butter a 10-inch ceramic tart pan and set aside.

In a large bowl, whisk egg yolks with ¾ cup sugar, vanilla, nutmeg and salt.

Pour the cream into a sauce pot, and set over medium heat until it simmers. Then turn off the heat.

Whisking the egg mixture feverishly, slowly pour the hot cream into the eggs. Whisk fast and pour slow to ensure you don't scramble the eggs. Whisk in the bourbon. Then pour the finished mixture into the prepared tart pan.

Bake for 30 minutes on the center rack. The edges should be firm and the very center should jiggle just a little. Remove the pan from the oven and cool until it’s easy to handle. Then cover the tops with plastic wrap and chill for at least 4 hours.

Before serving, sprinkle remaining ¼ cup sugar over the top of the custard.

Set the oven on broil and place a rack in the top position. Broil the sugar for 1–2 minutes until dark and caramelized. Don’t take your eyes off the pan. Sugar can turn black under the broiler in a matter of seconds. Once caramelized, crack the surface and serve!
