---
draft: false
title: "Rhubarb Custard Bars"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients  
  
2 cups all-purpose flour  
1/4 cup sugar  
1 cup cold butter  
FILLING:  
2 cups sugar  
7 tablespoons all-purpose flour  
1 cup heavy whipping cream  
3 large eggs, room temperature, beaten  
5 cups finely chopped fresh or frozen rhubarb, thawed and drained  
TOPPING:  
6 ounces cream cheese, softened  
1/2 cup sugar  
1/2 teaspoon vanilla extract  
1 cup heavy whipping cream, whipped  
Directions  
  
In a bowl, combine the flour and sugar; cut in butter until the mixture resembles coarse crumbs. Press into a greased 13x9-in. baking pan. Bake at 350° for 10 minutes.  
Meanwhile, for filling, combine sugar and flour in a bowl. Whisk in cream and eggs. Stir in the rhubarb. Pour over crust. Bake at 350° until custard is set, 40-45 minutes. Cool.  
For topping, beat cream cheese, sugar and vanilla until smooth; fold in whipped cream. Spread over top. Cover and chill. Cut into bars. Store in the refrigerator.  
  
Test Kitchen Tips  
Heavy whipping cream gives custard fillings a luscious, decadent texture, but half-and-half cream or whole milk can be substituted with nice results.  
This is a potluck hero: It makes a mountain of bars. If you're cooking for a smaller crew, however, just halve the recipe and use a 9- or 8-inch square baking dish.  
If you prefer your bars on the tart side, bump up the rhubarb to 6 or 6-1/2 cups or reduce the sugar in the filling to 1-1/2 cups.  
Go full garden party for your friends and family by adding the seeds of one vanilla bean or 1 teaspoon rosewater to the filling. Both flavors pair beautifully with rhubarb and take these already fab bars to another level.  
Nutrition Facts  
  
1 bar: 198 calories, 11g fat (7g saturated fat), 52mg cholesterol, 70mg sodium, 23g carbohydrate (16g sugars, 1g fiber), 2g protein.
