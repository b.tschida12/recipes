---
draft: false
title: "Root Beer Float Fudge"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
### **INGREDIENTS:**

-   3 cup granulated sugar

    > ¾ cup unsalted butter

    > 1 cup heavy cream

    > pinch of salt

    > 3 cup white chocolate morsels

    > 1½ cup marshmallow cream

    > 3 tsp root beer concentrate (I use McCormicks)

### **DIRECTIONS:**

1.  In large saucepan, heat sugar, butter, salt and cream until combined. Bring to a boil and stir continuously for 4 minutes.

2.  Remove from heat. Quickly stir in white chocolate and marshmallow. Whisk in vigorously until smooth, be patient this will take a couple minutes. Using an electric mixer yields best results.

3.  Pour half of mixture into a parchment paper lined 13x9 baking dish. To remaining fudge, whisk in root beer concentrate. Stir until combined. Pour over white fudge. Using a knife, swirl the two together. Refrigerate 4 hours or overnight. Cut into bite size pieces and enjoy. Tastes great cold!
