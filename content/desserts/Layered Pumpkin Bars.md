---
draft: false
title: "Layered Pumpkin Bars"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
TOTAL TIME: Prep: 40 min. + cooling Bake: 15 min. + chilling YIELD:15 servings

**Ingredients**

CRUST:  
1-1/2 cups graham cracker crumbs  
1/3 cup sugar  
1 teaspoon ground cinnamon  
1/3 cup butter, melted

CREAM CHEESE FILLING:  
12 ounces cream cheese, softened  
1 cup sugar  
3 eggs

PUMPKIN FILLING:  
1 can (15 ounces) solid-pack pumpkin  
3 eggs, separated  
3/4 cup sugar, divided  
1/2 cup 2% milk  
2 teaspoons ground cinnamon  
1/2 teaspoon salt  
1 envelope unflavored gelatin  
1/4 cup cold water

TOPPING:  
1 cup heavy whipping cream  
3 tablespoons sugar  
1/4 teaspoon vanilla extract

Directions  
1. Preheat oven to **350°**. In a large bowl, combine crumbs, sugar and cinnamon; stir in butter. Press into an ungreased 13x9-in. baking dish. In a large bowl, beat cream cheese until smooth. Beat in sugar and eggs until fluffy. Pour over crust. Bake 15-20 minutes or until set. Cool on a wire rack.  
2. In the top of a double boiler or a metal bowl over simmering water, combine pumpkin, egg yolks, 1/2 cup sugar, milk, cinnamon and salt. Cook and stir over low heat until a thermometer reads 160°; remove from heat. Transfer to a large bowl; wipe out double boiler.  
3. In a small saucepan, sprinkle gelatin over cold water; let stand 1 minute. Heat over low heat, stirring until gelatin is completely dissolved. Stir into pumpkin mixture; cool.  
4. In the double boiler, whisk egg whites and remaining sugar over low heat until temperature reaches 160°. Remove from heat; using a mixer, beat until stiff glossy peaks form and sugar is dissolved. Fold into pumpkin mixture. Pour over cream cheese layer. Cover and refrigerate for at least 4 hours or until set.  
5. Just before serving, in a large bowl, beat cream until it begins to thicken. Add sugar and vanilla; beat until stiff peaks form. Spread over pumpkin layer. Yield: 15 servings.

Nutritional Facts  
1 piece: 370 calories, 21g fat (12g saturated fat), 144mg cholesterol, 275mg sodium,

**41g carbohydrate** (33g sugars, 2g fiber), 6g protein.
