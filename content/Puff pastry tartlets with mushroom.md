---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Puff pastry tartlets with mushroom"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: "Caramelized onion bites with sautéed crimini mushrooms, balsamic caramelized onions, and applewood smoked gruyere cheese. Thee perfect little appetizers! They’re made with puff pastry and take no time at all to whip up! These are the perfect appetizers to serve your guests this holiday season."
tags: ["Appetizers"]
servings:
prep_time: 10 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 45
calories: # in kcal
---
## INGREDIENTS:  
- 4 tablespoons salted butter, divided  
- 2 tablespoons vegetable oil, divided  
- 5-6 ounces crimini or button mushrooms, sliced  
- 2 medium yellow onions, thinly sliced  
- 1/4 teaspoon dried thyme  
- 1 tablespoon sugar  
- 1 1/2 tablespoon balsamic vinegar  
- 1/4 teaspoon garlic powder  
- salt and pepper, to taste  
- 4 ounces grated gruyere cheese (I used applewood smoked gruyere)  
- 1 package (1 pound) frozen puff pastry, thawed  
- 1 egg, lightly beaten  

## DIRECTIONS:  
  
1. In a large cast iron skillet over medium heat, heat 1 tablespoon of oil along with 1 tablespoon of butter. Add the mushrooms and sauté for about 5 minutes until tender. Remove and set aside in a medium bowl.  
2. Heat the remaining 3 tablespoons of butter along with the 1 tablespoon of oil and sauté the onions for 5 minutes.
3. Add the dried thyme and sugar and continue to cook for 25 minutes on medium low heat. Make sure to stir the mixture every 5 minutes or so. If the onions are caramelizing too fast, turn down the heat a little more. 
4. Add the balsamic vinegar to deglaze the pan, allow the onions to soak it all in. 
5. Turn off heat. Add the onions to the mushroom mixture. 
6. Season with the garlic powder and salt and pepper to taste, stir to combine. 
7. Allow to cool to room temperature. Mixture can be kept in an air-tight container and refrigerated for up to 48 hours at this point. Allow to come to room temperature before proceeding. 
8. Add the gruyere cheese and mix well.  
9. Position two racks in the upper and lower thirds of the oven. Preheat the oven to 400 degrees F. Line 2 baking sheets with parchment paper or silicone mats.  
10. Using a knife or a pizza cutter, cut the puff pastry into 2-inch squares and place them on a lined baking sheet. Make sure the puff pastry is cold before proceeding. If not, allow it to rest in the refrigerator for 10-15 minutes before proceeding. 
11. Using a pastry brush, brush the beaten egg on the squares. 
12. Place a tablespoon of caramelized onion mixture in the center. Bake for 20-25 minutes, rotating the pan halfway in between until crisp and golden brown. Let cool slightly before serving.  

## NOTES:  
  
- Make sure the puff pastry is cold when it goes into the oven. If at any point it becomes soft, allow it to rest in the refrigerator for 15-20 minutes or until it firms up again. Do not open the oven door during the first 10 minutes of baking time. Frequently opening the oven may cause the puff pastry to not ‘puff up’.  
- Mozzarella or gouda cheese may be used to replace the gruyere. I really like the smoky flavor of applewood smoked gruyere with these bites.  
- If you’ve prepared the caramelized onion mixture ahead of time, you can zap it in the microwave for 30 seconds to bring it back to room temperature quickly.
