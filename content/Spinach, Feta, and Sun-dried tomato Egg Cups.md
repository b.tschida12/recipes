---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Spinach, Feta, and Sun-dried tomato Egg Cups"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings: 12
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 15-18 # in minutes
calories: # in kcal
---
## INGREDIENTS
- 4 eggs
- 3/4 cup egg whites
- 1/2 cup sun-dried tomatoes, drained and chopped
- 2 cups spinach, chopped
- 1/4 cup feta cheese, crumbled
- 1 teaspoon Italian seasoning
- salt and pepper to taste
- cooking spray

## INSTRUCTIONS
1. Preheat oven to 375 degrees. Coat a 12 cup muffin pan with cooking spray.
2. In medium bowl, mix eggs, egg whites, spinach, Italian seasoning, salt and pepper until blended.
3. Pour egg mixture into prepared muffin cups. Fill each cup about 3/4 of the way.
4. Sprinkle sun-dried tomatoes and feta cheese over egg mixture.
5. Bake for 15-18 minutes.
