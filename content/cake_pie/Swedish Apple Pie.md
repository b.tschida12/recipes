---
draft: false
title: "Swedish Apple Pie"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Swedish Apple Pie

TOTAL TIME: Prep: 15 min. Bake: 25 min.

YIELD: 8 servings.

Ingredients

1 cup + 1 T sugar

1/2 cup whole wheat flour

½ cup all-purpose flour

¾ c butter, melted

1/2 teaspoon ground cinnamon

1 large egg

1/4 teaspoon vanilla extract

2 medium tart apples, chopped

½ cup chopped walnuts or pecans, toasted

Confectioners' sugar, optional

Directions

1\. Place apples in a pie pan. Sprinkle with 1T sugar and cinnamon In a large bowl, combine the 1 c sugar butter then add in , flours. . In a small bowl, whisk egg and vanilla. Stir into dry ingredients just until moistened. and nuts. Spread over apples. .

2.. Bake at 350° for 25-30 minutes or until a toothpick inserted in the center comes out clean. Sprinkle with confectioners' sugar if desired. Serve warm.
