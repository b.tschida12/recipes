---
draft: false
title: "Rhubarb Coffee Cake"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**TOTAL TIME:** Prep: 30 min. Bake: 25 min. **YIELD:** 9 servings.

## **Ingredients**

## 2 cups diced fresh or frozen rhubarb

## 1/4 cup plus 2/3 cup sugar, divided

1/2 cup butter, softened

2 large eggs

1-1/2 teaspoons vanilla extract

1-1/2 cups all-purpose flour

1 teaspoon baking powder

1/2 teaspoon salt

1/8 teaspoon baking soda

3/4 cup buttermilk

2 tablespoons brown sugar

1/2 teaspoon ground cinnamon

## **Directions**

**1.** Preheat oven to 350°. In a small bowl, combine rhubarb and 1/4 cup sugar. In a large bowl, cream butter and remaining sugar until light and fluffy. Add eggs, one at a time, beating well after each addition. Stir in vanilla. In another bowl, whisk flour, baking powder, salt and baking soda; add to creamed mixture alternately with buttermilk, beating well after each addition. Fold in rhubarb mixture.

**2.** Pour into a greased 9-in. square baking pan. In a small bowl, mix brown sugar and cinnamon; sprinkle over batter. Bake 25-30 minutes or until a toothpick inserted in the center comes out clean. Serve warm or at room temperature.

-   
