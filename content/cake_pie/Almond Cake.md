---
draft: false
title: "Almond Cake"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
TOTAL TIME: Prep: 30 min. Bake: 50 min. + cooling YIELD:12 servings

Ingredients  
1 can (8 ounces) almond paste  
3/4 cup plus 1 tablespoon sugar, divided  
1/2 cup butter, softened  
3 large eggs, lightly beaten  
1 tablespoon orange liqueur  
1/4 teaspoon almond extract  
1/4 cup all-purpose flour  
1/4 teaspoon plus 1/8 teaspoon baking powder  
1/4 cup confectioners' sugar  
1 package (10 ounces) frozen sweetened raspberries, thawed

Directions  
1. Line an 8-in. round baking pan with parchment paper; coat paper with cooking spray and set aside.

2\. In a large bowl, combine the almond paste, 3/4 cup sugar and butter; beat for 2 minutes until blended. Beat in the eggs, liqueur and extract. Combine flour and baking powder; add to creamed mixture just until combined.

3\. Spread into the prepared pan. Bake at 350° for 40-45 minutes or until a toothpick inserted in the center comes out clean. Cool completely on a wire rack.

4\. Invert cake onto cake plate; remove parchment paper. Sprinkle with confectioners' sugar.

5\. Place raspberries in a food processor; cover and process until pureed. Strain, reserving juice; discard seeds. In a small saucepan over medium heat, cook raspberry juice and remaining sugar for 15-18 minutes or until mixture is reduced to 1/4 cup. Serve with cake.

Yield: 12 servings (1/4 cup sauce).

Nutritional Facts  
1 slice with 1 teaspoon sauce: 272 calories, 14g fat (6g saturated fat), 73mg cholesterol, 84mg sodium, 34g carbohydrate (28g sugars, 2g fiber), 4g protein.
