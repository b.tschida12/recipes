---
draft: false
title: "Cookie Crust Apple Pie"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Cookie Crust Deep-Dish Apple Pie

One of my favorite make-and-take desserts, this deep-dish pie is based on an old Hungarian recipe that's popular in this part of Ohio. The easy cookie crust is the best part. —Diane Shipley, Mentor, Ohio

TOTAL TIME: Prep: 45 min. + chilling Bake: 40 min.

YIELD: 15 servings.

Ingredients

1 cup butter, softened

1/2 cup sugar

1/2 teaspoon salt

3 large egg yolks

1 teaspoon vanilla extract

3 tablespoons sour cream

3 cups all-purpose flour

FILLING:

1 cup sugar

2 tablespoons all-purpose flour

1 teaspoon ground cinnamon

8 cups sliced peeled Granny Smith apples

2 tablespoons butter, cubed

TOPPING:

1 large egg white

1/4 cup finely chopped pecans

Directions

1\. Beat butter, sugar and salt until blended; beat in egg yolks and vanilla. Beat in sour cream. Gradually beat in flour. Divide dough in half. Shape each into a rectangle; wrap in plastic. Refrigerate until firm, about 1 hour.

2\. Preheat oven to 375°. On waxed paper, roll one half of dough into a 14x10-in. rectangle. Transfer to a greased 13x9-in. pan; press onto bottom and 1/2 in. up sides of pan. (Dough may crack; pinch together to patch.)

3\. For filling, mix sugar, flour and cinnamon; toss with apples. Place in crust; dot with butter. On waxed paper, roll remaining dough into a 14x10-in. rectangle; place over filling. Pinch edges to seal.

4\. Whisk egg white until frothy; brush over top. Sprinkle with pecans. Cut slits in top crust. Bake on a lower oven rack until golden brown, 40-45 minutes. Cool on a wire rack.
