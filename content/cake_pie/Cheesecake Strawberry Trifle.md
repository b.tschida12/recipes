---
draft: false
title: "Cheesecake Strawberry Trifle"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients

1 package (8 ounces) cream cheese, softened

1 cup sour cream

1/2 cup cold whole milk

1 package (3.4 ounces) instant vanilla pudding mix

1 carton (12 ounces) frozen whipped topping, thawed

1-1/2 cups crushed butter-flavored crackers (about 38 crackers)

1/4 cup butter, melted

2 cans (21 ounces each) strawberry pie filling

Directions

1\. In a large bowl, beat the cream cheese until smooth. Beat in the sour cream; mix well.

2\. In a small bowl, beat milk and pudding mix on low speed for 2 minutes. Stir into cream cheese mixture. Fold in whipped topping.

3\. In a small bowl, combine crackers and butter.

4\. In a 2-1/2-qt. trifle bowl, layer half of the cream cheese mixture, crumbs and pie filling. Repeat layers. Refrigerate until serving.
