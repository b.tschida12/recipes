---
draft: false
title: "Margaritaville Pie"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1 ½ c crushed pretzel sticks

¼ c sugar

¼ lb butter, melted

1 14 oz can sweetened condensed milk

⅓ c Key Lime juice

2 T Tequila

2 T Triple Sec

1 c heavy cream, whipped

Prepare crust by combining crushed pretzels and sugar; add melted butter. Press into a 9” buttered pie pan and chill

Prepare Filling, combine milk, lime juice, tequila and triple sec; blend well. Fold whipping cream into mixture.

Pour filling into chilled crust and freeze for 3-4 hours, or until firm.

Before serving, garnish each piece with a thin slice of lime (optional).

Can be stored in the freezer for several days
