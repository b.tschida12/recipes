---
draft: false
title: "Rhubarb Upside Down Cake"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients

3 cups sliced fresh or frozen rhubarb

1 cup sugar

2 tablespoons all-purpose flour

1/4 teaspoon ground nutmeg

1/4 cup butter, melted

BATTER

1/4 cup butter, melted

3/4 cup sugar

1 large egg, room temperature

1-1/2 cups all-purpose flour

2 teaspoons baking powder

1/2 teaspoon ground nutmeg

1/4 teaspoon salt

2/3 cup 2% milk

Sweetened whipped cream, optional

Directions

1\. Place rhubarb in a greased 10-in. cast-iron or other heavy ovenproof skillet. Combine sugar, flour and nutmeg; sprinkle over rhubarb. Drizzle with butter; set aside. For batter, in a large bowl, beat the butter and sugar until blended. Beat in the egg. Combine the flour, baking powder, nutmeg and salt. Gradually add to egg mixture alternately with milk, beating well after each addition.

2\. Spread over rhubarb mixture. Bake at 350° until a toothpick inserted in the center comes out clean, about 35 minutes. Loosen edges immediately and invert onto a serving dish. Serve warm. If desired, serve with whipped cream.
