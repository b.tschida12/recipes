---
draft: false
title: "Banana Coffee Cake"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Banana Coffee Cake  
  
Prep Time 15 minutes Cook Time 25 minutes Total Time 40 mins  
Serves serves 12  
Ingredients  
  
For the streusel  
  
1/2 cup brown sugar  
1/4 cup granulated sugar  
1/4 teaspoon salt  
1 tablespoons cinnamon  
1/2 cup butter, melted  
1 1/4 cups flour  
For the cake  
  
1 1/2 cups all purpose flour  
3/4 teaspoons baking soda  
3/4 teaspoons baking powder  
2 teaspoons cinnamon  
1/4 teaspoon salt  
1/2 cup brown cup sugar  
1/4 cup granulated sugar  
1 /2 cup (1 stick) butter, room temperature  
1 egg  
3 overripe bananas, about 12 ounces  
powdered sugar, for topping  
Instructions  
  
Preheat oven to 350 degrees. Spray a 9x9 baking dish with non-stick spray.  
To prepare the streusel, stir together the sugars, cinnamon, salt, and melted butter in a medium mixing bowl until well combined.  
Stir in the flour. Set aside while you prepare the cake batter.  
Add the flour, baking soda, baking powder, cinnamon, and salt to a large mixing bowl and whisk to combine.  
Add the sugars and butter to a medium mixing bowl and beat with a mixer until light and fluffy.  
Mash the bananas until smooth with a fork. Add to the sugar and butter along with the egg and stir until just combined.  
Add the banana mixture to the flour mixture and stir until just combined.  
Spread cake batter into prepared pan.  
Take the streusel topping in your hands and press together to form large crumbs. Top the cake with the streusel. It will seem like too much, but use it all for the best streusel experience.  
Bake for 25 minutes or until a tester inserted in the middle comes out mostly clean.  
Cool completely.  
Sprinkle the top with powdered sugar before serving.
