---
draft: false
title: "Orange Liqueur"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**<u>Recipe: Homemade Triple Sec Recipe</u>**

Ingredients

3 Tangerines (sliced 1/8″ thick)

1/2 Cup Tangerine Juice (from approximately 5 tangerines)

12 Ounces Everclear

12 Ounces Vodka

5 Cups Sugar

3 Cups Water

Orange Blossom Water

Instructions

Preheat oven to 200º On parchment paper (or silicone baking matte) lined sheet pan, lay out tangerine slices in a single layer. Bake for 1 1/2 hours. Tangerine slices should be somewhat shriveled, but still sticky (not dry).

Remove from oven and let cool.

Pour everclear into a sealable jar and add tangerine slices. Cover, shake the jar and set in a cool place.

The sliced tangerines should stay in the everclear for **8-12 hours** (shake occasionally).

In another sealable jar, pour vodka and tangerine juice. Cover, shake the jar and set in a cool place.

The juice and vodka should sit for **24 hours** (shake occasionally).

Strain the everclear through a fine mesh strainer. Strain again through a coffee filter and set aside.

Strain vodka and juice through a double layer of cheese cloth. Then strain through a coffee filter (you may need to do this straining in batches and use a new filter for each batch strained). Set aside.

In a large saucepan add the sugar and water.

Stir until sugar is dissolved and bring to a low boil for a minute. Take off heat and let cool to room temperature.

Combine sugar syrup, everclear mixture and vodka mixture into a very large bowl.

Add a drop or two of the orange flower water if you would like a little more orange aroma. (Go easy with this stuff…it’s really potent and if you add too much you could end up with stuff that tastes like perfume.)

Pour triple sec into sealable bottles.

May be stored in the refrigerator for a month.

**<u>Homemade orange liqueur</u>**

Ingredients

1/4 cup zest from 3 small naval oranges

1 tablespoon dried bitter orange peel

1 cup brandy

1 cup vodka

4 whole cloves

2 cups sugar

1 1/2 cups water

Directions

Combine zest, dried orange peels, brandy, and vodka in a small sealable container. Seal and shake. Let steep for 19 days at room temperature. On day 20, add the cloves, then seal and shake. Let steep for an additional day.

Bring sugar and water to a boil in a small saucepan over high heat stirring to dissolve. Let this simple syrup cool. Strain the contents of the jar through a fine mesh strainer and then through a coffee filter. Discard the solids. Combine the strained mixture with the simple syrup in a jar or bottle. Shake and let it rest for a minimum of one day before use. Store in a sealed container at room temperature for up to one year (it's best within three months).
