---
draft: false
title: "Tequila Drink Recipes"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**<u>Paloma</u>**

ice

50ml tequila blanco (we used El Jimador)

10ml agave syrup

10ml lime juice

60ml pink grapefruit juice

soda water

*For the garnish*

sea salt

wedge pink grapefruit

STEP 1

Dip the rim of a highball glass in a bowl of water, shake off the excess, then sprinkle a spoonful of salt onto a plate or saucer and dip the glass into that to coat the rim.

STEP 2

Fill a cocktail shaker with ice, then add all the remaining ingredients minus the soda water. Shake until the outside of the shaker feels cold, then strain into your prepared glass. Add a few fresh ice cubes and top with soda water to serve. Garnish with the wedge of grapefruit

**<u>Tequila Sunrise</u>**

2 tsp grenadine

ice

50ml tequila

1 tbsp triple sec

1 large orange, or 2 small ones, juiced

½ lemon, juiced

1 cocktail cherry

STEP 1

Pour the grenadine into the base of a tall glass and set aside. Fill a cocktail shaker with ice and add the tequila, triple sec and fruit juices. Shake until the outside of the shaker feels cold.

STEP 2

Add a few ice cubes to the serving glass then carefully double strain the cocktail into it, trying not to disturb the grenadine layer too much. Add more ice if needed to fill the glass then garnish with a cherry on a stick or cocktail umbrella.

**<u>Classic Margarita</u>**

ice

50ml tequila reposado

25ml lime juice

20ml triple sec (we used Cointreau)

*For the garnish*

salt

2 lime wedges

STEP 1

Sprinkle a few teaspoons of salt over the surface of a small plate or saucer. Rub one wedge of lime along the rim of a tumbler and then dip it into the salt so that the entire rim is covered.

STEP 2

Fill a cocktail shaker with ice, then add the tequila, lime juice and triple sec. Shake until the outside of the shaker feels cold.

STEP 3

Strain the mix into the prepared glass over fresh ice. Serve with a wedge of lime.

**<u>Tequila Diablo Rojo</u>**

ice

50ml tequila blanco (we used El Jimador)

15ml lime juice

15ml sugar syrup

125-150ml ginger beer

1-2 tsp crème de cassis to finish

*For the garnish*

1 tsp pomegranate seeds

STEP 1

Half-fill a tall glass with ice then add the tequila, lime and sugar syrup. Stir gently to combine then top up with ginger beer then stir once more.

STEP 2

Slowly pour the crème de cassis and garnish with a few pomegranate seeds dropped into the glass.

**<u>Negroni</u>**

ice

25ml mescal (we used Amores Espadin)

25ml red vermouth

25ml Campari

*For the garnish*

1 orange slice

STEP 1

Put a few ice cubes in a tumbler and pour over the mescal, vermouth and Campari. Stir until the outside of the glass feels cold.

STEP 2

Garnish with an orange slice to serve.
