---
draft: false
title: "Hot Buttered Rum Mix"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
-   **For the batter:**

-   1 pound butter

-   1 pound brown sugar

-   1 pound confectioners' sugar

-   1 quart vanilla ice cream, softened

-   1 Tbsp vanilla extract

-   1 Tbsp ground cinnamon

-   2 tsp ground nutmeg
