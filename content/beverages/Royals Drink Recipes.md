---
draft: false
title: "Royals Drink Recipes"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Royals DRINK RECIPES

**Banana Monkey**

1 ½ oz coffee liqueur

½ oz crème de cacao

1 oz coconut cream

2 oz Carnation milk

Half a banana

Note: Instead of crème de cacao you can also use Hershey’s chocolate sauce

Blend and serve in a large cocktail glass

**Bushwhacker**

3/4 oz White rum

3/4 oz Coffee liqueur

1/4 oz of dark cacao liqueur

1 oz of coconut cream

2 oz Carnation evaporated milk

Blend and serve in a cocktail glass with a melon and pineapple garnish. Enjoy!

**Princesa Maya (Mayan Princess)**

¾ oz brandy

¾ oz almond liqueur

1 oz strawberries

½ oz coconut cream

1 oz Carnation evaporated milk

¼ oz grenadine

One third of a banana

Blend, serve and enjoy!

**Leaping Lizard**

1 oz Melon liqueur

1 oz Banana liqueur

1 oz Kahlua

1 oz Pineapple juice

1 oz Carnation evaporated milk

¼ Banana

Blend, serve and enjoy!

**Weeping Willow**

1/2 oz Cuervo Tequila

1/2 oz 1912 Tequila Cream liqueur

1/2 oz Kahlua

Dash of chocolate sauce

1 oz Coconut cream

1 1/2 oz Carnation evaporated milk

Blend, serve and enjoy!

**Bahama Mama**

3/4 oz vodka

3/4 oz coffee liqueur

1 1/2 oz Carnation milk

1/4 oz grenadine

1/2 oz coconut cream

1/2 a banana

Mangolele

½oz vodka

1oz white rum

½oz Melon liqueur

1oz Kahlua

1½oz orange juice

2oz mango

¼oz grenadine

Place all ingredients in a blender, serve with ice; garnish glass with melon pearls and orange slices.

**Caribbean Breeze**

1oz melon liqueur

½oz banana liqueur

½oz coconut cream

2oz pineapple juice

2oz orange juice

Place all ingredients in a blender, serve with ice; garnish glass with melon pearls and orange slices.

**Tropicana**

1 1/4 oz coconut flavored rum

1 oz coconut cream

1 1/2 oz mango

2 oz pineapple juice

1/4 oz blue Curacao

Blend and serve. Cheers!

**Char Hut**

1 oz Bacardi Coconut Rum

1/2 oz Peach liqueur

1/2 oz Melon liqueur

4 oz Cranberry juice

1/2 oz Dark Rum or Spiced Rum

Pour Coconut rum and liqueurs over ice in a tall glass, add cranberry juice and top with dark or spiced rum, garnish with lime wedges. Cheers!

**Ticket to Fly**

1/2 oz white rum

1/2 oz vodka

1/2 oz gin

1/2 oz tequila blanco

4 oz piña colada cocktail mix

A dash of grenadine

Blend, serve and enjoy! As they say here in Mexico, salud!
