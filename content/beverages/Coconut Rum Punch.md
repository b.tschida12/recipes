---
draft: false
title: "Coconut Rum Punch"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
3-1/2 cups unsweetened pineapple juice

1-1/2 cups orange juice

1 cup coconut water

1 cup orange peach mango juice

1 cup coconut rum

1 cup dark rum

1/4 cup Key lime juice (regular lime juice will work, too)

3 tablespoons Campari liqueur or grenadine syrup

Just add all the ingredients to a large pitcher and stir. You can even do this a day in advance of your soiree to help the flavors marry a bit better.
