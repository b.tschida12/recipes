---
draft: false
title: "French Hot Chocolate"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
TOTAL TIME: Prep/Total Time: 15 min.

YIELD: 2 servings.

Ingredients

2/3 cup 2% milk

2 tablespoons heavy whipping cream

1 teaspoon brown sugar

1/2 teaspoon confectioners' sugar

1/8 teaspoon instant espresso powder, optional

2 ounces dark chocolate candy bar, chopped

Whipped cream and chocolate shavings

Directions

1\.

In a small saucepan, heat milk, cream, brown sugar, confectioners' sugar and if desired, espresso powder over medium heat until bubbles form around sides of pan. Remove from heat; whisk in dark chocolate until melted. Serve in mugs with whipped cream and chocolate shavings.
