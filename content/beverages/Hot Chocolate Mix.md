---
draft: false
title: "Hot Chocolate Mix"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**<u>Hot Chocolate Mix</u>** Makes 3 c (12, 1-c servings)

1 c sugar

6 oz unsweetened chocolate, chopped fine (Hershey’s)

1 c (3 oz) unsweetened cocoa powder

½ c (1.5 oz) nonfat dry milk powder

5 t cornstarch

1 t vanilla extract

¾ t kosher salt

Process all ingredients in food processor until ground to powder, 30-60 seconds.

To use:

Mix with heated milk; 1 c milk to ¼ c mix. Whisk constantly, simmering 2-3 minutes. Serve.

Store up to 2 months
