---
draft: false
title: "Mai Tai"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
**Don The Beachcomber Mai Tai Recipe**

1.5 oz Myer's Plantation Rum.

1 oz Cuban Rum.

.5 oz Cointreau.

.25 oz Falernum.

1 oz Grapefruit Juice.

3/4 oz Lime Juice.

2 dashes Angostura Bitters.

1 dash Pernod.

**The Duke's Mai Tai recipe:**

.4 ounce orgeat syrup

1/2 ounce orange curacao

1/2 ounce gold rum

3 ounces fresh PPOG (pineapple, passion orange, guava) juice

1 1/4 ounce dark rum

Garnish with a pineapple, lime and parasol

MAI TAI

In 1953 the Matson Navigation Company commissioned Victor Bergeron to create a drink for their new hotel The Royal Hawaiian. Trader crafted it for some friends who were visiting from Tahiti, who then cried out, “Maita’i” the Tahitian word for “good”. Trader Vic then made a variation on the Mai Tai recipe adding pineapple juice, which is still served today at the hotel.

**vic’s ‘44 – 15**

our take on trader vic’s original 1944 mai tai recipe made with pyrat xo reserve rum, myers’s jamaican dark rum, ferrand dry curacao, orgeat, and freshly squeezed lime juice, served with a spent lime shell

**bali tai – 15**

a curious topless take on a traditional mai tai, with its effortless blend of

fresh lime,

koloa rum,

lychee puree and

black pepper syrup

**96 degrees in the shade – 15**

cool off with this frozen delight …

captain morgan rum,

fresh pineapple,

passion fruit puree,

lime juice,

orgeat,

mint,

topped with a float of old lahaina dark rum

**white wash – 15**

topped with not one, but two floats!

old lahaina silver rum,

ferrand dry curacao,

orgeat,

fresh lime and

pineapple juices,

topped with a float of old lahaina dark rum

and our signature coco-loco foam

**ali’i mai tai – 35**

this signature mai tai is made with the finest aged rums … appleton 21 year aged rum from jamaica,

el dorado 15-year special reserve rum from guyana, ferrand dry curacao,

freshly muddled pineapple,

orgeat,

fresh lime juice,

topped with our signature coco-loco foam and bitters

**royal mai tai – 15**

the royal hawaiian, a luxury collection resort classic…this is our original trader vic mai tai recipe made with

fresh squeezed pineapple and

orange juice,

orange curacao,

orgeat and

local rums from the old lahaina distillery on maui

**choco-tai – 15**

so unique you have to try it!

selva rey cacao infused rum,

housemade kona coffee syrup,

fresh lemon juice,

bittermen’s elemakule tiki bitters,

fresh pineapple juice, with a

float of old lahaina dark rum

Build in shaker with ice:

1 oz. Bacardi Rum

1 tsp. Cherry Vanilla Puree

½ oz. Amaretto di Saronno

½ oz. Cointreau

1 oz. Fresh Govinda Orange Juice

2 oz. Fresh Govinda Pineapple Juice

½ oz. Whaler’s Dark Rum Float

Roll the shaker, pour in a large “bucket” glass. Float with Whaler’s Dark Rum, garnish with a parasol with cherry, pineapple and lime wedge.
