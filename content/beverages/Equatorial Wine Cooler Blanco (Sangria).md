---
draft: false
title: "Equatorial Wine Cooler Blanco (Sangria)"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
3 limes, halved and thinly sliced

2 oranges, halved and thinly sliced

½ pineapple, thinly sliced

1 mango or papaya peeled, seeded, thinly sliced

½ c superfine sugar

1 c lime juice (about 8 limes)

2 bottles cheap white wine

1 qt mango nectar (or other tropical fruit juice)

16-20 drinks
