---
draft: false
title: "Rosedale Fried Cabbage"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Saute:

1 chopped onion

½ chopped green pepper

1 t oil

Add Cabbage which has been cut into 1-½” pieces. Heat and stir for only 2 or 3 minutes. Be sure all cabbage is coated with oil

Then add a mix of:

2 T sugar

2 T white vinegar

1 T soy sauce

1 t salt

Cook just long enough to coat cabbage, onion and pepper.

Optional: Add 1 t poppy seed and ¼ t cayenne pepper.

Serve at once

\~ Mary Ellen Sehmel
