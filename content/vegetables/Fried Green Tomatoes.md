---
draft: false
title: "Fried Green Tomatoes"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
4 large, firm green tomatoes, cut crosswise into ½” slices

Salt, pepper

1 c finely ground cornmeal

1 t paprika

2 eggs

Vegetable oil

Sprinkle the tomato slices with the salt & pepper, set aside.

Combine the cornmeal and paprika in a shallow bowl. In another bowl, beat the eggs.

Coat the tomato slices in the egg, then dredge them in the cornmeal mixture.

Fry as many tomatoes as fit comfortably in the pan until nicely browned, about 2 minutes per side.

Transfer them to a paper towel-lined platter. Repeat until all tomatoes are cooked.

TIPS:

Use a cast-iron skillet, keep the temperature of the oil around 350-375

Pat the tomatoes dry before dredging
