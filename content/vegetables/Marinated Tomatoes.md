---
draft: false
title: "Marinated Tomatoes"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
TOTAL TIME: Prep: 10 min. + marinating

YIELD: 8 servings.

Ingredients

3 large or 5 medium fresh tomatoes, thickly sliced

1/3 cup olive oil

1/4 cup red wine vinegar

1 teaspoon salt, optional

1/4 teaspoon pepper

1/2 garlic clove, minced

2 tablespoons chopped onion

1 tablespoon minced fresh parsley

1 tablespoon minced fresh basil or 1 teaspoon dried basil

Directions

1\. Arrange tomatoes in a large shallow dish. Combine remaining ingredients in a jar; cover tightly and shake well. Pour over tomato slices. Cover and refrigerate for several hours.
