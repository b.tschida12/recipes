---
draft: false
title: "Mexican Street Corn (Elote)"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
How To Make Elote (Mexican Street Corn)

INGREDIENTS

6 medium fresh ears of corn

1/2 cup mayonnaise

1/2 cup Mexican crema or sour cream

2 tablespoons lime juice (from 1 medium lime)

1/2 teaspoon chili powder

1/2 teaspoon ground cumin

1 cup crumbled cotija cheese (Parmesan will work if you can't find cotija)

1 medium lime, cut into wedges, for serving

INSTRUCTIONS

Husk the corn: If your corn still has husks, remove them and the silk from the corn, but keep as much of the stalk end attached as possible.

Prepare a grill: Heat an outdoor gas grill to high or prepare a charcoal grill for indirect heat. A grill pan over medium heat will work as well.

Make the sauce: While the grill heats, place the mayonnaise, crema or sour cream, lime juice, chili powder, and cumin in a medium bowl and whisk until smooth. Transfer this sauce into a tall, narrow drinking glass. Put the cotija on a large plate.

Grill the corn: Grill the corn uncovered until some the kernels are bright yellow and a few are charred, 2 to 3 minutes per side, for a total of 12 to 15 minutes. Remove the corn from the grill and cool for 5 minutes.

Dip the corn: Hold the drinking glass of sauce at an angle, then dip a cooled ear of corn into the mixture, turning the cob to coat completely. Hold the cob over the glass and shake gently to let the excess sauce drip back into the glass.

Roll in the cheese: Roll the dipped cob in the crumbled cheese to coat completely. Place on a serving platter.

Repeat and serve: Repeat dipping and rolling the remaining corn in the sauce and cheese.

Serve with lime wedges for squeezing over the corn.
