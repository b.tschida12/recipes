---
draft: false
title: "Roasted garlic cauliflower"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1/2 cup butter melted

2 garlic cloves minced

1 cup Italian or plain breadcrumbs

1/2 cup grated Parmesan cheese

1/4 tsp salt

1/4 tsp black pepper

1 medium cauliflower head

Instructions

Preheat oven to 400 degrees F. Line a large baking sheet with parchment paper. Set aside.

Remove all leaves from cauliflower head. Cut cauliflower into florets, all roughly the same size. You can slice the large florets in half, if needed.

Melt butter and in a small bowl. Add garlic and stir in.

Place breadcrumbs, salt, pepper and Parmesan cheese in another bowl.

Dip each cauliflower piece into butter first, then to breadcrumbs.

Place each breaded piece on prepared baking sheet. Repeat until you use up all cauliflower.

Roast cauliflower for 35 to 32 minutes, or until the breading is golden brown.
