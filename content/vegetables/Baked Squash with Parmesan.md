---
draft: false
title: "Baked Squash with Parmesan"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients

4 cups thinly sliced yellow summer squash (3 medium)

3 tablespoons olive oil

1/2 teaspoon salt

1/2 teaspoon pepper

1/8 teaspoon cayenne pepper

3/4 cup panko (Japanese) bread crumbs

3/4 cup grated Parmesan cheese

Directions

Preheat oven to 450°. Place squash in a large bowl. Add oil and seasonings; toss to coat.

In a shallow bowl, mix bread crumbs and cheese. Dip squash in crumb mixture to coat both sides, patting to help coating adhere. Place on parchment paper-lined baking sheets. Bake 20-25 minutes or until golden brown, rotating pans halfway through baking.

Nutrition Facts

2/3 cup: 137 calories, 10g fat (2g saturated fat), 7mg cholesterol, 346mg sodium, 8g carbohydrate (0 sugars, 2g fiber), 5g protein.
