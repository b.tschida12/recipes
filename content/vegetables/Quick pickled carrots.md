---
draft: false
title: "Quick pickled carrots"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
8 Carrots – peeled, then can be grated, julienned, or sliced into ribbons with a vegetable peeler. If you buy pre-grated carrots, you just saved yourself 10 minutes

1/2 cup rice vinegar

1/4 cup sugar

2 tbls sesame oil

1 tsp salt

Place carrots in a small bowl or large mason jar.

Whisk all other ingredients together.

Pour over carrots. Mix to coat.

Soak for at least an hour, up to a week. They get more delicious the longer they sit. Store in refrigerator.
