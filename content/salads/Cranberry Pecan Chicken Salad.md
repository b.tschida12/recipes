---
draft: false
title: "Cranberry Pecan Chicken Salad"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
> <img src="media/image1.jpg" style="width:2.16146in;height:2.16146in" />

Cranberry Pecan Chicken Salad with Poppy Seed Dressing

**Prep Time** 20 mins **Cook Time** 10 mins **Total Time** 30 mins

**Ingredients**

**Dressing**

-   1/2 cup mayonnaise

-   1/4 cup sour cream

-   2 tablespoon honey softened

-   1 tablespoon Dijon mustard

-   1 tablespoon poppy seeds

-   salt to taste

**Salad**

-   4 cups chicken breast , cooked, chopped (about 2 chicken breasts)

-   1 cup pecans , chopped

-   1/2 cup dried cranberries

-   4 green onions , chopped

-   **Instructions**

    1.  In a medium bowl, combine all dressing ingredients. Whisk until well combined. Add salt, to taste.

    2.  In a large bowl, combine all salad ingredients. Add salad dressing and toss to coat. Add salt, to taste.
