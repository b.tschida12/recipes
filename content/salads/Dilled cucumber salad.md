---
draft: false
title: "Dilled cucumber salad"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
2 English cucumbers, thinly sliced  
1 teaspoon salt  
1-1/2 cups (12 ounces) sour cream  
1/4 cup thinly sliced red onion  
1/4 cup snipped fresh dill  
2 tablespoons white wine vinegar  
2 garlic cloves, minced  
1 teaspoon sugar  
1 teaspoon coarsely ground pepper
