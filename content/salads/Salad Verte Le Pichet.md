---
draft: false
title: "Salad Verte Le Pichet"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients

1 cup orange juice

1 small shallot

1/2 cup sherry vinegar

1/3 cup organic Duchilly hazelnuts (for the viniagrette)

1 tablespoon Dijon mustard

Approx. 1 cup soy oil

Salt and fresh ground black pepper to taste

1 head organic Bibb lettuce

1/2 cup organic Duchilly hazelnuts (for garnishing the salad)

Preparation

For the Vinaigrette: Heat the orange juice to a boil in a small saucepan. Reduce to 1/2 cup. Cool. Toast the hazelnuts and cool. Peel and roughly chop the shallot.

Put the reduced orange juice, shallot, sherry vinegar, hazelnuts, and Dijon mustard in the blender. Season with salt and pepper. Blend at high speed until homogenous.

With the blender running, add the oil slowly until vinaigrette is the consistency of heavy cream. The actual amount of oil required may be slightly more or less. Do not let the blender run too long, as the vinaigrette will break if it gets too warm.

Taste the vinaigrette to adjust the seasoning. Add more salt or pepper if needed. If the vinaigrette is too acidic, add more oil with the blender running. If the vinaigrette gets to thick, thin with a bit of water. Note that this recipe makes about 2 cups vinaigrette, which would be enough for quite a few salads.

For the Salad: Remove any dead or damaged leaves from the lettuce. Wash the lettuce carefully, making sure to check the interior leaves near the base for dirt. Let drain in the refrigerator for about an hour. Toast the hazelnuts, cool and chop roughly.

When ready to serve, remove the core from the lettuce. Separate the leaves and tear them if they are too large. Make sure that there is no water on the leaves (spin in a salad spinner if necessary to remove any water).

Toss the lettuce with salt, ground fresh pepper and a little vinaigrette. DO NOT OVERDRESS. Taste a bit to check seasoning and amount of vinaigrette. Correct as necessary. Arrange the lettuce on individual salad plates or one salad dish. Sprinkle the chopped hazelnuts over the top.

Serve immediately.
