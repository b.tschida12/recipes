---
draft: false
title: "Jícama Apple Salad with Orange-Mustard Dressing"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
½ c orange juice

2 T fresh lime juice (1 lime)

2 T cider vinegar

2 T mustard

2 T brown sugar

¼ c roughly chopped mint

Salt, pepper to taste

1 jicama, peeled & cut into matchsticks

3 apples, cut into matchsticks

1 red onion, peeled, halved, thinly sliced

Combine dressing ingredients in s bowl

Combine apples, jicama and onion in a small bowl. Add dressing, toss well to coat.

Keeps, refrigerated up to 3 days

Serves 4-6
