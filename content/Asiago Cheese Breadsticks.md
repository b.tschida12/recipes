---
draft: false
date: 2021-01-25T20:16:56-05:00
title: "Asiago Cheese Breadsticks"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings: 20
prep_time: 60 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 20-25# in minutes
calories: # in kcal
---
## Ingredients:

- 20 Rhodes Texas Rolls or 30 Rhodes Yeast Dinner Rolls, thawed to room temperature
- 1/2 cup butter melted
- 1-2 cups grated Asiago cheese
- dried parsley, if desired

## Instructions:

1. Stretch and roll each Texas roll or 1 1/2 dinner rolls combined into a 14-inch rope (rolls will relax and end up being about 11-inches long).
2. Place butter in shallow bowl. Dip each rope in butter and place horizontally on an 11x17-inch jelly roll pan.
3. Drizzle any remaining butter over rolls. Sprinkle cheese over top and then parsley, if desired. Cover with plastic wrap and let rise until double in size, about 45-60 minutes.
4. Remove wrap and bake at 350°F 20-25 minutes. Cover with foil last 5-10 minutes of baking if necessary to prevent cheese from over browning. Serve warm.
