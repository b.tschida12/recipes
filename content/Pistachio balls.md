---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Pistachio balls"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings:
prep_time: 20 # in minutes
cook: false # true->when cooking; false->when cooling
cook_time: # in minutes
calories: # in kcal
---
## Ingredients  
- 1 4oz package of sundried tomato goat cheese  
- 1/2 cup de-shelled pistachios  
- salt to taste  

## Instructions  
1. Cut your goat cheese into 7 slices and form into balls  
2. Use a mortar and pestle to lightly crush your pistachios (don't grind them, whack 'em)  
3. Add salt to the pistachio mixture to taste  
4. Roll your balls around in the pistachio mixture to cover  
5. Once they're all covered, roll one more time in the leftover pistachio dust  
6. Enjoy!
