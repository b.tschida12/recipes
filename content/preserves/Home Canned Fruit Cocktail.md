---
draft: false
title: "Home Canned Fruit Cocktail"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Fruits can be mixed together in pan prior to filling jars or if you wish to get a pretty look, layer into sterilized jars.

Peel and cube - pears, peaches, add pineapple chunks and grapes.

Pour hot syrup over fruit; seal and put in hot water bath long enough for bubbles to come up in the jar.

Jars may be set in a cake pan and placed in oven (several inches of water in pan). 300 degrees - 10 or 15 min. Will seal the jars.

Recipe: Grace Fogle (Woodrow School Cookbook)
