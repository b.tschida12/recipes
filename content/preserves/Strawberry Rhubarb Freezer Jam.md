---
draft: false
title: "Strawberry Rhubarb Freezer Jam"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients

1 1/4 c crushed strawberries (buy about 1 pt. fully ripe strawberries)

1/2 c finely chopped rhubarb (buy about 1/2 lb. rhubarb)

4 c sugar, measured into separate bowl

1 pkg certo fruit pectin (comes in a pouch. not package)

1 Tbsp fresh lemon juice

Directions

1\. RINSE clean plastic containers and lids with boiling water. Dry thoroughly.

2\. MEASURE crushed strawberries and finely chopped rhubarb into large bowl. Stir in sugar. Let stand 10 minutes, stirring occasionally.

3\. MIX pectin and lemon juice in small bowl. Add to fruit mixture; stir 3 minutes or until sugar is dissolved and no longer grainy. (A few sugar crystals will remain.)

4\. FILL all containers to within 1/2 inch of tops. Wipe off top edges of containers; immediately cover with lids. Let stand at room temperature 24 hours. Jam is now ready to use.

Store in refrigerator up to 3 weeks or freeze extra containers up to 1 year. Thaw in refrigerator before using.
