---
draft: false
title: "Grandmother_s Mustard Pickles"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1 qt small cucumbers

1 qt large cucumbers, sliced or diced

2 qts small pearl onions

1 qt green tomatoes, sliced

2 qts celery, cut in 1 inch pieces

3 small heads cauliflower, torn apart

6 green peppers, cut in strips

Prepare vegetables and soak overnight in a strong brine, made by adding 2 c salt to each gallon of water.

In morning, bring to a boiling point and simmer until vegetables are tender.

Drain thoroughly in a colander. Place in kettle and cover with the following dressing:

Mix together

1-½ c sugar

4 T flour

4 T powdered mustard

½ t turmeric

1 T celery salt

Add slowly 3 pints hot vinegar - stir til smooth.

Cook in double boiler til it thickens, pour over hot vegetables. Simmer for 4 minutes.

Pack in hot, clean jars.

Recipe: Grace Fogle
