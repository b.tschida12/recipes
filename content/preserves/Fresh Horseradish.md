---
draft: false
title: "Fresh Horseradish"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1 Horseradish root (1 lb)

1 c white vinegar

1 t salt

½ t sugar

1 small turnip

Scrub and peel root, cut away dark parts. Cut in cubes (3 cups).

Place vinegar, salt, sugar in blender. Add ⅓ each of horseradish and turnip. Blend til smooth.

Keep in refrigerator in covered jars.

Recipe: Diane Fogle (Woodrow School Cookbook)
