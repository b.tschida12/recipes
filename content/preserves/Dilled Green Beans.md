---
draft: false
title: "Dilled Green Beans"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Pack beans in pint jars with sliver of garlic, dill, and ½ t mustard seed.

Boil:

5 c vinegar

5 c water

¼ c salt

Pour over beans, boil in jars for 5 minutes and seal at once.

Recipe: Hazel Greenly - Grandmother of Bev Buck. Woodrow School Cookbook)
