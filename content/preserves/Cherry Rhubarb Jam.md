---
draft: false
title: "Cherry Rhubarb Jam"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Yield: 4-½ c jam

Ingredients

2 1/2 tablespoons (half a 1 3/4-oz. package) Sure-Jell pectin labeled "For less or no sugar needed recipes"

1 1/2 cups sugar, divided

2 1/2 cups coarsely chopped Bing cherries (from about 1 lb. fruit)

1 cup chopped rhubarb

1 1/4 cups unsweetened cherry juice\*

1 1/2 tablespoons lemon juice

1/4 teaspoon butter (prevents foaming)

Combine pectin and 1/4 cup sugar in a 5- to 6-qt. pot. Stir in cherries and rhubarb, then cherry and lemon juices and butter. Bring mixture to a brisk boil over high heat, stirring often.

Add remaining 1 1/4 cups sugar. Return jam to a brisk boil, stirring. Cook, stirring constantly, 1 minute. Remove from heat.

Ladle jam into heatproof jars and close with lids.

Let cool to room temperature, inverting jars occasionally to distribute fruit.

Keeps, chilled, up to 1 month.

\*We used Trader Joe's Just Cherry juice.
