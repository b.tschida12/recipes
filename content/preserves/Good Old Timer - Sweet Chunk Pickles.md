---
draft: false
title: "Good Old Timer - Sweet Chunk Pickles"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
5 lbs medium cucumbers (about 10)

8 c sugar

4 c vinegar

2 T pickling spice

1 T + 1 t salt

Pour boiling water over the whole cucumbers. Let stand overnight.

Drain, bring water to a boil and pour over cukes again for 3 more days, 4 in all.

On the 5th day, drain and cut cukes into chunks, ¾” or so. Combine sugar, vinegar, spice and salt. Bring to boil and pour over the cuke chunks.

Cover and let stand for 2 days, then bring the mixture to boiling and pack into hot jars. Fill to ½” of top. Place tops on jars and process in hot water bath for 5 minutes (start counting time when jars go into hot water bath).

Remove and cool and ENJOY.

Recipe: Mrs. H.A. Martin (Woodrow School cookbook)
