---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Smoked Salmon Cheeseball"
author: "Cyndi Ingle"
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings:
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
## Ingredients
- 2 8 oz. cream cheese, softened
- 1 8 oz. can sliced waterchestnuts, drained and chopped fine
- 1/2 small onion, chopped fine
- 1/2 lb smoked salmon, shredded or minced (I buy this in the refrigerator case at the fish counter in the grocery store - it is in a vacuum sealed wrap, fresh and moist)
- 1/2 tsp garlic powder
- 1 tsp dill weed
- sliced almonds or other chopped nuts

## Directions
1. Beat cream cheese with mixer until fluffy.
2. Add the water chestnuts, onion, salmon & spices. Mix together well.
3. Cover & refrigerate until the mixture becomes firm enough to handle.
4. Separate the mixture and roll into two or more balls. Roll each ball in the sliced almonds or chopped nuts as desired.
5. Cover each cheeseball with plastic wrap and keep refrigerated.

## Notes
- Serve with a variety of crackers.
- When I double this recipe (always do when I make it around the holidays) I use a total of 5 packages of cream cheese.
- Cooking Tip: You can make 2 large cheeseballs with this recipe, or several smaller ones.
