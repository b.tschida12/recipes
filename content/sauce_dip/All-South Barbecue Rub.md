---
draft: false
title: "All-South Barbecue Rub"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
From Thrill of the Grill

2 T salt

2 T sugar

2 T brown sugar

2 T ground cumin

2 T chili powder

2 T freshly cracked black pepper

1 T cayenne pepper

4 T paprika

Makes about 1 cup of rub
