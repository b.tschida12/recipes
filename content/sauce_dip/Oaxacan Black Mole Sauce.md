---
draft: false
title: "Oaxacan Black Mole Sauce"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Yield: makes 14 cups

Time: 2 hours, 30 minutes

Ingredients

1 cup vegetable oil

4 ancho chiles (3½ oz.), seeded

6 chilhuacle negro chiles (3½ oz.) (or substitute cascabel), seeded

3 pasilla chiles (13/4 oz.), seeded

2⁄3 cup white sesame seeds

1 Tbsp. plus 1 tsp. kosher salt, divided, plus more as needed

1⁄4 cup dried Mexican oregano

1 tsp. dried thyme

1⁄4 tsp. whole allspice berries

6 whole black peppercorns

3 whole cloves

One 3-in. cinnamon stick

1 medium yellow onion, coarsely chopped (1¼ cups)

6 medium garlic cloves, peeled

3⁄4 cup whole almonds

1⁄4 cup María Mexican cookies, or substitute animal crackers

6 oz. ripe plantains, peeled and coarsely chopped (1 cup)

1 medium apple, cored and cut in 1-in. cubes (1 cup)

2 3⁄4 oz. fresh pineapple, peeled, cored, and cut in 1-in. cubes (1/3 cup)

3⁄4 cup raisins

2 1⁄3 cups coarsely chopped Roma tomatoes

2 dried avocado leaves

1⁄2 cup sugar, plus more as needed

7 oz. Oaxacan chocolate, finely chopped (1 cup)

4 1⁄2 cups chicken stock

Instructions

To a large skillet over medium heat, add the oil. When the oil is hot, add the ancho, chilhuacle negro, and pasilla chiles and fry, turning frequently, until they are toasted and crispy, 5–7 minutes. Turn off the heat and transfer the chiles to a wire rack to cool completely. Transfer the oil to a heatproof measuring cup and set aside.

In a medium pot, bring 8 cups water to a boil. Add the chiles, turn off the heat, cover, and set aside until the chiles are hydrated and soft, about 30 minutes.

To a large cast-iron skillet over medium heat, add the sesame seeds and 1 teaspoon salt and cook, stirring frequently, until toasted, about 2 minutes. Transfer the seeds to a medium bowl and set aside. To the skillet, add the oregano, thyme, allspice, peppercorns, cloves, and cinnamon stick, and cook, stirring frequently, until fragrant, about 5 minutes. Transfer the mixture to a mortar and pestle, pulverize until finely ground, and set aside. To the skillet, add the onion and garlic. Cook, stirring frequently, until lightly charred all over, about 10 minutes. Transfer to the bowl with the sesame seeds and set aside.

To the skillet, over medium heat, add ½ cup of the reserved oil. When hot, fry the following ingredients individually, until each is deeply toasted and aromatic: almonds (about 5 minutes), cookies (2–3 minutes), plantains (4–6 minutes), apple (3–5 minutes), pineapple (5–7 minutes), and raisins (2–3 minutes). Use a slotted spoon to transfer them all to a large, heatproof bowl as they are finished. Set aside.

To a saucepan over medium heat, add the tomatoes and ½ cup water. Bring to a simmer, cover, and cook until the tomatoes have softened, 10–12 minutes. Remove from the heat and set aside.

Use a slotted spoon to transfer the chiles to a blender, then add 1½ cups of their soaking liquid. Blend until very smooth. Place a fine mesh strainer over a large bowl and pass the chile mixture through it, pressing on the mixture with the back of a small ladle or a silicone spatula to achieve a fine purée. Discard any solids that remain.

Transfer the strainer to a second medium bowl. To the same blender, add the sesame seeds, onion, garlic, ground spice mixture, almonds, cookies, plantains, apples, pineapple, and raisins. Add 1 cup cold water, then blend until very smooth. Pass this mixture through the strainer as well, discarding any solids that remain.

Transfer the strainer to a third medium bowl. To the same blender, add the tomatoes with their juices and blend until very smooth. Pass the tomato purée through the strainer as well, discarding any solids.

To a large pot over medium heat, add the remaining ½ cup oil. When the oil is hot, add the chile paste and cook without disturbing until bubbly, about 5 minutes. Stir in the seed and spice mixture, then continue cooking 5 minutes more. Stir in the tomato purée, bring to a simmer, then add the avocado leaves, sugar, chocolate, and remaining 1 tablespoon salt. Continue cooking at a simmer, stirring occasionally, until the chocolate has completely melted, 8–10 minutes. Stir in the chicken stock, bring the mole back up to a boil, then season with additional salt or sugar as needed. Use immediately, or transfer the pot to an ice bath to cool quickly, transfer to airtight containers, and refrigerate for up to 1 week or freeze for up to 3 months.
