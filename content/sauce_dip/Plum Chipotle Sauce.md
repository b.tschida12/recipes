---
draft: false
title: "Plum Chipotle Sauce"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients

1 lb. firm-ripe plums (about 4 medium), pitted and quartered

1/2 cup chopped yellow onion

2 Tbs. balsamic vinegar

2 Tbs. cider vinegar

1/4 cup packed dark brown sugar

1 Tbs. minced garlic

1 tsp. minced seeded canned chipotle chile in adobo sauce

1 tsp. kosher salt

1/8 tsp. ground cloves

Nutritional Information

Preparation

Put all of the ingredients in a heavy-duty 3-quart saucepan and bring to a boil. Turn the heat down to medium, cover, and simmer, stirring occasionally, until the plums break down, about 10 minutes. Uncover and simmer until thick, 5 to 7 minutes.

Purée the sauce in a blender until smooth. Use or cool to room temperature and refrigerate in an airtight container for up to 1 week.

From Fine Cooking
