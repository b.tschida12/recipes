---
draft: false
title: "Green Goddess Whipped Feta Dip"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
You can replace some of the cilantro or parsley with other tender herbs—basil, dill, chives. Just make sure to include the tarragon; it gives the dip an anise-y, bittersweet punch.

Ingredients

MAKES ABOUT 2 CUPS

8 ounces feta

1 cup (or more) buttermilk

2 cups cilantro leaves with tender stems

2 cups parsley leaves with tender stems, plus sprigs for serving

½ cup tarragon leaves

4 oil-packed anchovy fillets, chopped (optional)

2 tablespoons finely grated lemon zest

1 tablespoon fresh lemon juice

Kosher salt, freshly ground pepper

Olive oil (for drizzling)

Mixed crudité, such as radishes, halved if large, endive, leaves separated, small carrots, scrubbed, trimmed, and/or baby fennel (for serving)

Preparation

Step 1

Process feta and buttermilk in a food processor until smooth. Add cilantro, parsley, tarragon, anchovies (if using), and lemon zest. Continue to process, adding more buttermilk if needed, until mixture is smooth and pale green with small flecks of herbs still visible. Add lemon juice, season with salt and pepper, and process just to combine.

Step 2

Transfer dip to a medium bowl. Drizzle with oil and top with a few sprigs of parsley. Serve on a platter with crudité.

Do Ahead: Dip can be made 1 day ahead. Cover and chill.
