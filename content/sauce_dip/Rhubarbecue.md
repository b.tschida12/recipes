---
draft: false
title: "Rhubarbecue"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients  
1-1/2 teaspoons salt  
1-1/2 teaspoons paprika  
1 teaspoon coarsely ground pepper  
3 to 4 pounds boneless country-style pork ribs

SAUCE:  
1-½ cups sliced fresh or frozen rhubarb (about 4 stalks)  
1cup fresh strawberries, halved  
2 to 3 tablespoons olive oil  
1 medium onion, chopped  
1 cup packed brown sugar  
3/4 cup ketchup  
1/2 cup red wine vinegar  
1/2 cup bourbon  
1/4 cup reduced-sodium soy sauce  
1/4 cup honey  
2 tablespoons Worcestershire sauce  
2 teaspoons garlic powder  
1 teaspoon crushed red pepper flakes  
1 teaspoon coarsely ground pepper

Directions  
1. Preheat oven to 325°. Mix salt, paprika and pepper; sprinkle over ribs. Refrigerate, covered, while preparing sauce.  
2. In a large saucepan, combine rhubarb and strawberries; add water to cover. Bring to a boil. Cook, uncovered, until rhubarb is tender, 8-10 minutes. Drain; return to pan. Mash until blended.  
3. In a Dutch oven, heat 1 tablespoon oil over medium heat. Brown ribs in batches, adding additional oil as needed. Remove from pan.  
4. Add onion to same pan; cook and stir until tender, 4-6 minutes. Add remaining ingredients; stir in rhubarb mixture. Return ribs to pan, turning to coat. Bring to a boil. Cover and bake until ribs are tender, 2 hours. Bake, uncovered, until sauce is slightly thickened, 30-35 minutes.
