---
draft: false
title: "Vanilla from vanilla beans"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
1 cup alcohol 80 proof\*

2-3 Vanilla Beans

Split beans lengthwise and place in a sterile glass jar with a lid.

Fill with 1 cup alcohol

Tighten the lid and give it a good shake. Put it in a cool, dark spot. Shake every 5 days or so and in 8 weeks it will be ready.

I usually use one jar to make it in and then pour it in another to use it. I pretty much start a new batch as soon as I pour the old into my plastic vanilla container. (I've been making it since my kids were little and liked to help in the kitchen - so plastic was the way to go.) It doesn't expire. The beans can be reused, but I add a new one or two every so often. Also, you can use it right from the jar you made it in. The recipe says that you can top it off at 3/4 full. I just change jars because I'd rather just start a new batch in the glass jar that happens to be the perfect size.

\*vodka is recommended for the purest Vanilla flavor, but you can also use bourbon, brandy or rum for an extra rich extract) I usually make it with vodka, but have used bourbon. It was very rich and added a distinctive flavor to my chocolate chips cookies.
