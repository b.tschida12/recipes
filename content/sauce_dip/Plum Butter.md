---
draft: false
title: "Plum Butter"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Plum Butter (Pflaumenmus)  
  
Calories: 45 kcal  
  
Author: Kimberly Killebrew  
  
Instructions  
  
Place the plums in the slow cooker. Stir in the sugar, cinnamon and cloves. Cook either on LOW for at least 10 hours or on HIGH for at least 4 hours until the plums are very soft.  
  
Use an immersion blender (or transfer to a blender, puree and return to the slow cooker) to puree the plum butter until smooth. If you prefer it a little chunky, blend until the desired texture is achieved.  
  
Continue to simmer the plum butter, this time with the lid opened, until the plum butter is reduced in volume to a spreadable texture, approximately 5-6 more hours on LOW or 3-4 hours on HIGH. (Note: The longer you simmer the plum butter the more deeply caramelized it will be. If you're going to simmer it for a much longer time, I recommend doing so on LOW to prevent burning. I’ll often simmer it with the lid closed for up to 20 hours and then open the lid, puree it, and simmer for another 5-6 hours until thickened.) Once it's reached the desired consistency, taste it. You can add more sugar at this point if you prefer and let it simmer until the sugar is dissolved.  
  
Spread this on toast, bagels, muffins, incorporate it into your favorite BBQ sauce or savory chicken/pork glaze.  
  
For Canning: Pour the hot plum butter into sterilized jars leaving 1/4 inch headspace. Wipe the rim and secure the lids. Boil in a water bath canner for 10 minutes. Remove the jars and let them sit undisturbed for 24 hours before transferring them to a cool, dark place for long-term storage. Will keep opened in the fridge for up to 2 months.
