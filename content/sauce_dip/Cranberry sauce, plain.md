---
draft: false
title: "Cranberry sauce, plain"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Homemade Cranberry Sauce

PREP TIME:

5 Minutes

COOK TIME:

15 Minutes

SERVINGS:

4 Servings

INGREDIENTS

1 bag (12 Oz. Bag) Cranberries

1 cup Cranberry Juice (or Orange, Apple Or Any Ot¬her Juice Combination)

1 cup Pure Maple Syrup (not Pancake Syrup!)

3 Tablespoons Juice (you Could Also Do Orange Zest, Lemon Zest, Lemon Juice – Anything Citrusy)

INSTRUCTIONS

Wash bag of cranberries under cool water, then dump into a medium saucepan.

Pour in 1 cup of cranberry juice (or whatever juice you choose).

Pour in 1 cup maple syrup.

Add orange juice (you could also do orange zest, lemon zest, lemon juice – anything citrusy).

Stir together and turn heat on high until it reaches a boil.

Once it comes to a rolling boil, turn the heat down to medium low and continue cooking over lower heat for about 10 minutes, or until the juice is thick. Turn off the heat.
