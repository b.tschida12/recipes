---
draft: false
title: "Balsamic Glaze"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
2 c balsamic vinegar

½ c brown sugar

Mix together in sauce pan over medium heat, stirring constantly until sugar dissolves. Bring to a boil, reduce heat to low, simmer until glaze is reduced by half, about 20 minutes. Glaze should coat the back of a spoon.

Let cool and pour into jar with a lid. Store in refrigerator.

Drizzle on cheese, grilled or roasted vegetables, salmon or pork. Strawberry
