---
draft: false
title: "Cherry Sauce"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients

16 ounces cherries (frozen or fresh, pitted)

1/2 cup granulated sugar

1/2 cup water

1 tablespoon cornstarch

1 tablespoon lemon juice (fresh)

In a medium saucepan, bring the cherries, sugar, and water to a boil over medium-high heat, stirring often.

In a small bowl, stir the lemon juice and cornstarch together until smooth.

Whisk it into the boiling cherry mixture.

Cook until the liquid has thickened, which should take about 1 more minute.

Remove the pot from the heat and taste. You can add a little extra sugar or lemon juice if needed at this point, depending on your personal preference.

Allow the sauce to cool to room temperature and serve over ice cream or cheesecake.

Return to a boil, stirring constantly. You don't want this sauce to scorch on the bottom.

used bg a 1/4c of Kraken rum, 1/4c water, and a teaspoon of vanilla extract, and an 1/8tsp of cinnamon with this recipe, and a can of tart cherries in water

4 1/2 cups pitted fresh (or frozen, thawed) Bing cherries (about 20 ounces)

1 cup brandy or orange juice

1/2 cup sugar

Bring all ingredients to a boil in a large heavy saucepan; reduce heat to medium-low. Simmer until cherries are softened and start to release juices, about 10 minutes. Using a slotted spoon, transfer cherries to a medium heatproof bowl.

Simmer juices until thick enough to coat the back of a spoon, 15–20 minutes. Pour reduced syrup over cherries. Serve warm.
