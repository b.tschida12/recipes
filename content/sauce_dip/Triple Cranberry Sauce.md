---
draft: false
title: "Triple Cranberry Sauce"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
INGREDIENTS

1 cup frozen cranberry juice cocktail concentrate, thawed

1/3 cup sugar

1 12-ounce package fresh or frozen cranberries, rinsed, drained

1/2 cup dried cranberries (about 2 ounces)

3 tablespoons orange marmalade

2 tablespoons fresh orange juice

2 teaspoons minced orange peel

1/4 teaspoon ground allspice

PREPARATION

Combine cranberry juice concentrate and sugar in heavy medium saucepan. Bring to boil over high heat, stirring until sugar dissolves. Add fresh and dried cranberries and cook until dried berries begin to soften and fresh berries begin to pop, stirring often, about 7 minutes. Remove from heat and stir in orange marmalade, orange juice, orange peel and allspice. Cool completely. Cover; chill until cold, about 2 hours. (Can be made 3 days ahead. Keep refrigerated.)
