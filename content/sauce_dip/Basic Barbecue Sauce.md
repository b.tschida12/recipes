---
draft: false
title: "Basic Barbecue Sauce"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
2 large onions, chopped

1-½ T vegetable oil for sautéing

1 16 oz. can tomato sauce

2 16 oz can crushed tomatoes

¾ c apple cider vinegar

¾ c dark brown sugar

¼ c sugar

1 T salt

1 T freshly cracked black pepper

1 T paprika

1 T chili powder

¼ c molasses

½ c orange juice

1 T liquid smoke

4 T Dijon mustard

In a large, heavy-bottomed saucepan, sauté onions in oil over medium-high heat until golden brown, about 7-10 minutes

Add all remaining ingredients, bring to a boil, then reduce heat and simmer, uncovered, at lowest possible heat for 4 hours. (Removes acidity)

Purée

Options:add after the 4 hours cooking time

Mexican

½ t ground cumin

½ t chili powder

2 T lime juice (about 1 lime)

10 sprigs cilantro

Asian:

1 t minced ginger

4 T soy sauce

2 T rice wine vinegar

1 T sugar

1 T sesame oil

Caribbean:

1 t brown sugar

2 T pineapple juice

2 T dark rum

2 T Caribbean hot sauce

Juice of 1 small orange

Pinch of allspice

Honey mustard

½ c Dijon mustard

4 T honey

Mediterranean

2 T balsamic vinegar

2 T red wine vinegar

1 T minced garlic

1 T olive oil

½ tomato, chopped

2 T lemon juice

½ c combined fresh herbs

(Basil, thyme, sage, parsley, oregano or rosemary)
