---
draft: false
title: "Cranberry Cherry Balsamic Relish"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Cranberry-Cherry Balsamic Relish

My absolute lip-puckering favorite. It packs a serious tart punch, it's absurdly easy, and worth making double for the leftover sandwiches alone.

1 12-oz bag cranberries, picked over

1 cup light brown sugar

½ large orange, peeled and segmented

1 cup dried cherries

1 1/2 tablespoons balsamic vinegar

1 tsp cinnamon

Place dried cherries and orange in food processor bowl; process to small bits. Add cranberries, brown sugar, balsamic vinegar and cinnamon, then pulse to finely chopped (some coarse spots are fine). Taste for seasoning and adjust to your own taste. May be made several days ahead; flavor deepens over time.
