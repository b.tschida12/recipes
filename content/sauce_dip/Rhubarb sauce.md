---
draft: false
title: "Rhubarb sauce"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
INGREDIENTS

2 cups water

2 even cups sugar

3 pounds rhubarb cut into 1-inch pieces

INSTRUCTIONS

Make a syrup with the water and sugar. Boil together a few minutes.

Add rhubarb to syrup when it starts boiling in center. Watch closely and let boil just 1 minute.

Now here is the trick that makes ordinary rhubarb sauce a Sauce Deluxe: Pour into a bowl or pan with a tight cover. Leave tightly covered until cold, and you will find a sauce very different from any you have ever eaten.
