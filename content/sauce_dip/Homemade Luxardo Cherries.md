---
draft: false
title: "Homemade Luxardo Cherries"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
#### 1 lb sour cherries, washed & pitted

#### 1 c sugar

#### ½ c water

#### 1 small cinnamon stick

#### 1 vanilla bean, split but not scraped

#### 1-½ T lemon juice

#### ¼ c Luxardo liqueur

1.  Sterilize 2 pint-size mason jars + lids, metal rings in boiling water for 15 minutes, then leave in hot water while cherries are prepared.

2.  In a medium saucepan, combine sugar and water. Heat over medium heat until sugar dissolves. Add in cinnamon stick and vanilla bean and continue to heat until mixture reaches 220 degrees

3.  Turn off heat and remove vanilla bean and cinnamon stick. Discard.

4.  Stir in lemon juice and Luxardo liqueur until combined.

5.  Carefully ladle the cherries and syrup into sterile jars. Wipe off any spillage from the rims of the jars with a paper towel dipped in hot water. Place the lids on and then screw the rings on until they’re just tight.

6.  Allow the jars to cool to room temperature. They should make a popping noise as they seal. Once the jars are cool, check to make sure that they have, in fact, sealed. If for some reason the lid didn’t, just put the jar in the ridge and use it within two weeks or so. Sealed jars will keep for a while.
