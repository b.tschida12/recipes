---
draft: false
date: 2021-01-25T20:16:56-05:00
title: "Bacon-wrapped Jalapeños"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings:
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: ~15 # in minutes
calories: # in kcal
---
## Ingredients

- 12 jalapeños, halved and seeded
- ½ c cream cheese
- ½ c Cheddar cheese, shredded
- 12 oz bacon

## Directions

1. Preheat oven to 400 degrees F (200 degrees C). Line a baking sheet with aluminum foil.
2. Mix cream cheese and Cheddar cheese together in a bowl until evenly blended. Fill each jalapeno half with the cheese mixture. Put halves back together and wrap each stuffed pepper with a slice of bacon. Arrange bacon-wrapped peppers on the prepared baking sheet.
3. Bake in the preheated oven until bacon is crispy, about 15 minutes.
