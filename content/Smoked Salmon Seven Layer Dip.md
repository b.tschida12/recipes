---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Smoked Salmon Seven Layer Dip"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings:
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
## Ingredients
- 4 ounces cream cheese
- 2 ounces fresh goat cheese
- 3 tablespoons prepared beet horseradish
- 2 teaspoons finely grated lemon zest
- 1 pound hot-smoked salmon, skin removed, flaked, divided
- 2 large radishes, trimmed, finely chopped
- ⅓ cup finely chopped red onion
- ½ cup drained capers, chopped
- 3 tablespoons finely chopped chives
- 2 Belgian endives, leaves separated
- Bagel chips or pumpernickel bread (for serving)

## Preparation

1. Line a 6"-diameter ring mold, 6"-diameter cake pan or springform pan, or 16-oz. ramekin with plastic wrap, pressing it along the bottom and up the sides (you can also use a small bowl, but the layers won’t be quite as even). Pulse cream cheese, goat cheese, horseradish, and lemon zest in a food processor until well combined and creamy.
2. Press a third of smoked salmon into an even layer across the bottom of mold. Spread half of cream cheese mixture evenly over salmon, smoothing surface with a rubber spatula.
3. Toss radishes and onion in a small bowl to combine, then scatter over cream cheese mixture and press down lightly into mixture. Top with half of remaining salmon, making an even layer, then scatter capers over. Spread remaining cream cheese mixture over capers and finish with a final layer of the remaining salmon. Cover dip with plastic wrap and chill at least 1 hour to let dip set and flavors meld.
4. Uncover dip and carefully invert onto a plate. Remove ring mold, then carefully peel away plastic. Top with chives. Serve with endives and bagel chips for spreading over.
5. Do Ahead: Dip can be made 1 day ahead. Keep chilled.
