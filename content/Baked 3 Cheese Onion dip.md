---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Baked 3 Cheese Onion dip"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings: 8
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 20-25 # in minutes
calories: # in kcal
---
## Ingredients

2 tablespoons vegetable oil
1 medium onion, finely chopped
Kosher salt
8 ounces cream cheese, room temperature
½ cup mayonnaise
2 teaspoons cornstarch
4 ounces sharp white cheddar, coarsely grated (about 1 cup), divided
2 ounces Monterey Jack, coarsely grated (about ½ cup), divided
Freshly ground black pepper
2 tablespoons finely chopped chives
2 tablespoons finely chopped peperoncini
Crackers and/or tortilla chips (for serving)

## Preparation

1. Preheat oven to 375°. 
2. Heat oil in a medium skillet over medium. Cook onion, stirring occasionally, until golden brown and softened, 8–10 minutes; season with salt. Let cool.
3. Pulse cream cheese and mayonnaise in a food processor until smooth.
4. Toss cornstarch, ¾ cup cheddar, and ¼ cup Monterey Jack in a medium bowl to coat cheese. 
5. Mix in cream cheese mixture and cooked onion; season with salt and pepper. 
6. Scrape into a 1-qt. baking dish and top with remaining cheddar and Monterey Jack. 
7. Bake dip until golden and bubbling, 20–25 minutes. Let dip cool 5 minutes, then top with chives and peperoncini. Serve with crackers.
