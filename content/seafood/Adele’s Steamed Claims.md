---
draft: false
title: "Adele’s Steamed Claims"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
2T - ⅓ c butter, melted in pan

1 minced garlic

Minced celery

Minced parsley

¼ c white wine

Add steamers. Cover. Steam until open
