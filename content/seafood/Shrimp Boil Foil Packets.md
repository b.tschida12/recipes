---
draft: false
title: "Shrimp Boil Foil Packets"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Ingredients

1 pound shrimp (31 to 35 count, peeled and deveined)

1 pound small red potatoes or gold potatoes (cut into small cubes, about 2 inches)

2 ears of corn on the cob (husked, cleaned, chopped into small rounds)

1 (12-ounce) smoked sausage (such as kielbasa) or andouille (sliced into 2 to 3 inch long pieces)

1/2 yellow or red onion (medium-sized, thinly sliced)

2 cloves garlic (minced)

4 tablespoons butter (melted)

1 teaspoon salt

1/2 teaspoon pepper

2 tablespoon Old Bay seasoning (store-bought or homemade)

Garnish: 2 tablespoon flat-leaf parsley (finely chopped)

Garnish: 1 tablespoon Old Bay seasoning (store-bought or homemade)

Garnish: 2 lemons (sliced, about 12 wedges)

Garnish: 1 stick butter (melted)

In a large bowl combine the drained potatoes and corn, the sausage, the thawed shrimp, the onion, and the garlic. Drizzle with the 4 tablespoons of melted butter, season with salt, pepper, and Old Bay seasoning. Mix very well to coat all ingredients.

Divide the shrimp boil ingredients between the four aluminum foil sheets.

Neatly fold into packets.

Place them onto a baking sheet and bake for 15 minutes. If grilling, place the packets directly onto the grill surface and cook for 15 minutes, flipping once after about 8 minutes.

Serve the packets slightly opened up, garnished with chopped parsley, a pinch of Old Bay seasoning and a few lemon wedges. Serve the 1 stick melted butter as a side for optional drizzling over the shrimp boil or dipping.
