---
draft: false
title: "Teriyaki Salmon"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Baked Teriyaki Salmon

Flaky and tender salmon with a delicious homemade teriyaki sauce, baked to perfection for an easy healthy dinner option.

Prep Time 10 minutes. Cook Time 15 minutes

Total Time 25 minutes

Servings 4

Calories 316kcal

Ingredients

4 Skinless Salmon Fillets about 6 oz each

1 Green Onion sliced

1 Tbsp Sesame Seeds

Teriyaki Sauce:

⅓ Cup Low-sodium soy sauce

¼ Cup Fresh orange juice

2-3 Tbsp Water

2 Tbsp Honey

2 Garlic Cloves minced

1 Tbsp Minced fresh ginger

1 Tsp Chili Flakes or to taste

½ Lemon juiced

½ Tbsp Gluten-free flour

Instructions

Preheat the oven to 400 degrees Fahrenheit.

In a small bowl whisk together all teriyaki sauce ingredients. Spray a baking dish with non-stick cooking spray and place the salmon in.

Pour the sauce over then bake in the preheated oven for 12-15 minutes until cooked through and flaky.

Once the salmon is cooked, remove from the oven, and spoon over with the thickened sauce from the baking dish.

Garnish with sesame seeds and green onions. Enjoy!

Nutrition

Calories: 316kcal \| Carbohydrates: 16g \| Protein: 36g \| Fat: 12g \| Saturated Fat: 2g \| Cholesterol: 94mg \| Sodium: 794mg \| Potassium: 940mg \| Fiber: 1g \| Sugar: 11g \| Vitamin A: 277IU \| Vitamin C: 16mg \| Calcium: 50mg \| Iron: 2mg
