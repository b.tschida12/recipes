---
draft: false
title: "Greek Salmon in foil packets"
author: 
recipe_image:
image_width: 512
image_height: 512
tagline: 
servings: 
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 1-10 # in minutes
calories: # in kcal
---
Mediterranean Salmon in Foil Packets

Prep Time. 10 mins

Cook Time. 25 mins

Total Time. 35 mins

Servings: 4 servings

Calories: 395 kcal

Author: The Seasoned Mom

Ingredients

4 salmon fillets about 6 ounces each

1/2 cup prepared pesto

1 onion chopped

1 pint grape tomatoes halved

½ cup crumbled feta cheese

Instructions

Heat oven to 350F (180C). Spray 4 large pieces of aluminum foil with cooking spray.

Place each salmon fillet on top of a piece of aluminum foil. Top each fillet with about 2 tablespoons pesto, ¼ cup of onion, ¼ of the tomatoes, and approximately 2 tablespoons of feta cheese. There's no need to measure the ingredients -- just use as much or as little as you like!

Seal the aluminum foil packets by folding them over the fish and pinching tightly to close. Place the foil packets on a large baking sheet and bake for approximately 25 minutes. Fish is done when it flakes easily with a fork.
