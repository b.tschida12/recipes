---
date: 2021-01-25T20:16:56-05:00
draft: false
title: "Balsamic Soy Roasted Mushrooms"
author: 
recipe_image: images/defaultImage.png
image_width: 512
image_height: 512
tagline: 
tags: ["Appetizers"]
servings:
prep_time: 20 # in minutes
cook: true # true->when cooking; false->when cooling
cook_time: 20 # in minutes
calories: # in kcal
---
## Ingredients
2 pounds mushrooms
1 tablespoon oil
3 tablespoons balsamic vinegar
2 tablespoons soy sauce (or tamari)
3 cloves garlic, chopped
1/2 teaspoon thyme, chopped
salt and pepper to taste

## Direction
- Toss the mushrooms in the oil, balsamic vinegar, soy sauce, garlic, thyme, salt and pepper
- Arrange in a single layer on a baking pan and roast in a preheated 400F/200C oven until the mushrooms are tender, about 20 minutes, mixing half way through.
